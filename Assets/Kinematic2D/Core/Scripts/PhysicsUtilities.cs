﻿using UnityEngine;

namespace Lightbug.Kinematic2D.Core
{
    using Lightbug.CoreUtilities;

    /// <summary>
    ///	Replacement for the RayCastHit and RayCastHit2D structs
    /// </summary>
    public struct CollisionHitInfo
    {
        public bool collision;
        public GameObject gameObject;
        public float distance;
        public Vector3 point;
        public Vector3 normal;

        public void Reset()
        {
            collision = false;
            gameObject = null;
            distance = 0;
            point = Vector3.zero;
            normal = Vector3.up;
        }

    }

    public enum RaySelectionRule
    {
        Shortest,
        ShortestNonZero,
        Longest
    }

    public static class PhysicsUtilities
    {
        private static RaycastHit2D boxcastHitInfo2D;
        private static RaycastHit2D[] boxcastHitInfos2D;
        private static RaycastHit2D[] raycastHitInfos2D;

        public static CollisionHitInfo Raycast(GameObject obj, bool is3D, Vector3 origin, Vector3 castDirection, float castDistance, LayerMask layerMask)
        {
            return is3D ?
            Raycast3D(origin, castDirection, castDistance, layerMask) :
            Raycast2D(obj, origin, castDirection, castDistance, layerMask);
        }

        public static CollisionHitInfo Boxcast(GameObject obj, bool is3D, Vector3 boxCenter, Vector3 boxSize, Vector3 castDirection, float castDistance, Vector3 boxUp, LayerMask layerMask)
        {
            return is3D ?
            Boxcast3D(boxCenter, boxSize, castDirection, castDistance, boxUp, layerMask) :
            Boxcast2D(obj, boxCenter, boxSize, castDirection, castDistance, boxUp, layerMask);
        }

        public static CollisionHitInfo BoxCastAll(GameObject obj, bool is3D, Vector3 boxCenter, Vector3 boxSize, Vector3 castDirection, float castDistance, Vector3 boxUp, RaycastHit2D[] results2D, RaycastHit[] results3D, LayerMask layerMask)
        {
            return is3D ?
            BoxcastAll3D(boxCenter, boxSize, castDirection, castDistance, boxUp, results3D, layerMask) :
            BoxcastAll2D(obj, boxCenter, boxSize, castDirection, castDistance, boxUp, results2D, layerMask);
        }

        // ---------------------------------------------------------------------------------------------------------
        // 2D ------------------------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------
        static CollisionHitInfo Raycast2D(GameObject obj, Vector3 origin, Vector3 castDirection, float castDistance, LayerMask layerMask)
        {
            CollisionHitInfo hitInfo = new CollisionHitInfo();
            hitInfo.Reset();

            raycastHitInfos2D = Physics2D.RaycastAll(
                origin,
                castDirection,
                castDistance,
                layerMask
            );
            System.Array.Sort(raycastHitInfos2D, (x, y) => x.distance.CompareTo(y.distance));

            for (int i = 0; i < raycastHitInfos2D.Length; ++i)
            {
                if (raycastHitInfos2D[i].collider != null && raycastHitInfos2D[i].collider.gameObject != obj)
                {
                    hitInfo.collision = raycastHitInfos2D[i].collider != null;

                    if (hitInfo.collision)
                        hitInfo.gameObject = raycastHitInfos2D[i].collider.gameObject;

                    hitInfo.distance = raycastHitInfos2D[i].distance;
                    hitInfo.point = raycastHitInfos2D[i].point;
                    hitInfo.normal = raycastHitInfos2D[i].normal;

                    return hitInfo;
                }
            }

            return hitInfo;
        }

        static CollisionHitInfo Boxcast2D(GameObject obj, Vector3 boxCenter, Vector3 boxSize, Vector3 castDirection, float castDistance, Vector3 boxUp, LayerMask layerMask)
        {
            CollisionHitInfo hitInfo = new CollisionHitInfo();
            hitInfo.Reset();

            boxcastHitInfos2D = Physics2D.BoxCastAll(
                boxCenter,
                boxSize,
                Utilities.SignedAngle(Vector2.up, boxUp, Vector3.forward),
                castDirection,
                castDistance,
                layerMask
            );
            System.Array.Sort(boxcastHitInfos2D, (x, y) => x.distance.CompareTo(y.distance));

            for (int i = 0; i < boxcastHitInfos2D.Length; ++i)
            {
                if (boxcastHitInfos2D[i].collider != null && boxcastHitInfos2D[i].collider.gameObject != obj)
                {
                    hitInfo.collision = boxcastHitInfos2D[i].collider != null;

                    if (hitInfo.collision)
                        hitInfo.gameObject = boxcastHitInfos2D[i].collider.gameObject;

                    hitInfo.distance = boxcastHitInfos2D[i].distance;
                    hitInfo.point = boxcastHitInfos2D[i].point;
                    hitInfo.normal = boxcastHitInfos2D[i].normal;

                    return hitInfo;
                }
            }

            return hitInfo;
        }

        static CollisionHitInfo BoxcastAll2D(GameObject obj, Vector3 boxCenter, Vector3 boxSize, Vector3 castDirection, float castDistance, Vector3 boxUp, RaycastHit2D[] results, LayerMask layerMask)
        {
            CollisionHitInfo hitInfo2D = new CollisionHitInfo();
            hitInfo2D.Reset();

            int hits = Physics2D.BoxCastNonAlloc(
                boxCenter,
                boxSize,
                Utilities.SignedAngle(Vector2.up, boxUp, Vector3.forward),
                castDirection,
                results,
                castDistance,
                layerMask
            );

            for (int i = 0; i < hits; i++)
            {
                boxcastHitInfo2D = results[i];
                if (boxcastHitInfo2D.collider != null && boxcastHitInfo2D.distance != 0 && boxcastHitInfo2D.collider.gameObject != obj)
                {
                    hitInfo2D.collision = boxcastHitInfo2D.collider != null;

                    if (hitInfo2D.collision)
                        hitInfo2D.gameObject = boxcastHitInfo2D.collider.gameObject;

                    hitInfo2D.distance = boxcastHitInfo2D.distance;
                    hitInfo2D.point = boxcastHitInfo2D.point;
                    hitInfo2D.normal = boxcastHitInfo2D.normal;

                    break;
                }
            }

            return hitInfo2D;

        }

        // ---------------------------------------------------------------------------------------------------------
        // 3D ------------------------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------

        static CollisionHitInfo Raycast3D(Vector3 origin, Vector3 castDirection, float castDistance, LayerMask layerMask)
        {
            CollisionHitInfo hitInfo = new CollisionHitInfo();
            hitInfo.Reset();

            RaycastHit hitInfo3D;

            Physics.Raycast(
                origin,
                castDirection,
                out hitInfo3D,
                castDistance,
                layerMask
            );

            hitInfo.collision = hitInfo3D.collider != null;

            if (hitInfo.collision)
                hitInfo.gameObject = hitInfo3D.collider.gameObject;

            hitInfo.distance = hitInfo3D.distance;
            hitInfo.point = hitInfo3D.point;
            hitInfo.normal = hitInfo3D.normal;

            return hitInfo;

        }

        static CollisionHitInfo Boxcast3D(Vector3 boxCenter, Vector3 boxSize, Vector3 castDirection, float castDistance, Vector3 boxUp, LayerMask layerMask)
        {
            CollisionHitInfo hitInfo = new CollisionHitInfo();
            hitInfo.Reset();

            RaycastHit hitInfo3D;

            Physics.BoxCast(
                boxCenter,
                boxSize / 2,
                castDirection,
                out hitInfo3D,
                Quaternion.LookRotation(Vector3.forward, boxUp),
                castDistance,
                layerMask
            );

            hitInfo.collision = hitInfo3D.collider != null;

            if (hitInfo.collision)
                hitInfo.gameObject = hitInfo3D.collider.gameObject;

            hitInfo.distance = hitInfo3D.distance;
            hitInfo.point = hitInfo3D.point;
            hitInfo.normal = hitInfo3D.normal;

            return hitInfo;

        }

        static CollisionHitInfo BoxcastAll3D(Vector3 boxCenter, Vector3 boxSize, Vector3 castDirection, float castDistance, Vector3 boxUp, RaycastHit[] results, LayerMask layerMask)
        {
            CollisionHitInfo hitInfo = new CollisionHitInfo();
            hitInfo.Reset();

            //RaycastHit hitInfo3D;

            int hits = Physics.BoxCastNonAlloc(
                boxCenter,
                boxSize / 2,
                castDirection,
                results,
                Quaternion.LookRotation(Vector3.forward, boxUp),
                castDistance,
                layerMask
            );

            for (int i = 0; i < hits; i++)
            {
                RaycastHit currentHitInfo3D = results[i];
                if (currentHitInfo3D.collider != null && currentHitInfo3D.distance != 0)
                {
                    hitInfo.collision = currentHitInfo3D.collider != null;

                    if (hitInfo.collision)
                        hitInfo.gameObject = currentHitInfo3D.collider.gameObject;

                    hitInfo.distance = currentHitInfo3D.distance;
                    hitInfo.point = currentHitInfo3D.point;
                    hitInfo.normal = currentHitInfo3D.normal;

                    break;
                }
            }

            return hitInfo;

        }


        public static CollisionHitInfo RaycastSweep(GameObject obj, bool is3D, Vector3 start, Vector3 end, float numberOfRays, Vector3 castDirection, float castDistance, RaySelectionRule rule, LayerMask layerMask)
        {
            CollisionHitInfo hitInfo = new CollisionHitInfo();
            hitInfo.Reset();

            float castArea = Vector3.Magnitude(end - start);
            Vector3 startToEndDirection = (end - start).normalized;

            hitInfo.distance = castDistance;

            CollisionHitInfo currentHitInfo;

            float step = castArea / (numberOfRays - 1);

            for (int i = 0; i < numberOfRays; i++)
            {
                Vector3 rayOrigin = start + startToEndDirection * step * i;

                // Debug.DrawRay( rayOrigin , castDirection * castDistance , Color.magenta );	

                currentHitInfo = PhysicsUtilities.Raycast(
                    obj,
                    is3D,
                    rayOrigin,
                    castDirection,
                    castDistance,
                    layerMask
                );


                if (!currentHitInfo.collision)
                    continue;

                switch (rule)
                {
                    case RaySelectionRule.Shortest:

                        if (currentHitInfo.distance < hitInfo.distance)
                            hitInfo = currentHitInfo;

                        break;

                    case RaySelectionRule.ShortestNonZero:

                        if (currentHitInfo.distance != 0 && currentHitInfo.distance < hitInfo.distance)
                            hitInfo = currentHitInfo;
                        break;

                    case RaySelectionRule.Longest:

                        if (currentHitInfo.distance > hitInfo.distance)
                            hitInfo = currentHitInfo;

                        break;
                }

            }

            return hitInfo;


        }

    }

}
