﻿using UnityEngine;
using Lightbug.Kinematic2D.Core;

namespace Lightbug.Kinematic2D.Implementation
{
    using Lightbug.CoreUtilities;

    public enum ActionType
    {
        SetVelocityY,
        SetAccelerationY,
        SetVelocityX,
        SetAccelerationX
    }

    public class VelocityArea : MonoBehaviour
    {
        [SerializeField]
        protected LayerMask layerMask = 0;

        [SerializeField]
		protected ActionType actionType = ActionType.SetVelocityY;

        [SerializeField]
		protected bool positiveValue = true;

        [Range_NoSlider(true)]
        [SerializeField]
		protected float value = 4;

        [SerializeField]
		protected bool forceNotGroundedState = true;

        [Tooltip("When the character enters the area automatically change its state to the Normal State.\n" +
        "(note: this is useful to avoid states that fully control the velocity, like for example the dash state, " +
        "if this field is disabled the dash movement will be inmune to the velocity area)")]
        [SerializeField] bool setNormalState = true;

		protected bool isActive = false;


		protected CharacterMotor targetCharacterMotor = null;
		protected CharacterController2D targetCharacterController = null;

		public virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (!Utilities.BelongsToLayerMask(other.gameObject.layer, layerMask))
                return;

            targetCharacterMotor = other.GetComponent<CharacterMotor>();
            if (targetCharacterMotor != null)
            {
                targetCharacterController = other.GetComponent<CharacterController2D>();
                isActive = true;
            }

        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (!Utilities.BelongsToLayerMask(other.gameObject.layer, layerMask))
                return;

            targetCharacterMotor = other.GetComponent<CharacterMotor>();
            if (targetCharacterMotor != null)
            {
                targetCharacterController = other.GetComponent<CharacterController2D>();
                isActive = false;
            }

        }


        void OnTriggerEnter(Collider other)
        {
            if (!Utilities.BelongsToLayerMask(other.gameObject.layer, layerMask))
                return;

            targetCharacterMotor = other.GetComponent<CharacterMotor>();
            if (targetCharacterMotor != null)
            {
                targetCharacterController = other.GetComponent<CharacterController2D>();
                isActive = true;
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (!Utilities.BelongsToLayerMask(other.gameObject.layer, layerMask))
                return;

            targetCharacterMotor = other.GetComponent<CharacterMotor>();
            if (targetCharacterMotor != null)
            {
                targetCharacterController = other.GetComponent<CharacterController2D>();
                isActive = false;
            }
        }

        protected void DoAction(CharacterMotor characterMotor)
        {
            if (characterMotor == null) return;

            if (forceNotGroundedState)
                characterMotor.ForceNotGroundedState();

            if (setNormalState)
                targetCharacterController.MovementController.SetState(MovementState.Normal);

            float sign = positiveValue ? 1 : -1;
            switch (actionType)
            {
                case ActionType.SetVelocityX:
                    characterMotor.SetVelocityX(sign * value);
                    break;

                case ActionType.SetVelocityY:
                    characterMotor.SetVelocityY(sign * value);
                    break;

                case ActionType.SetAccelerationX:
                    characterMotor.AddVelocityX(sign * value * Time.deltaTime);
                    break;

                case ActionType.SetAccelerationY:
                    characterMotor.AddVelocityY(sign * value * Time.deltaTime);
                    break;
            }

        }


        public virtual void Update()
        {
            if (!isActive)
                return;

            DoAction(targetCharacterMotor);

        }
    }

}