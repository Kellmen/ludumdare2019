﻿using Lightbug.CoreUtilities;
using Lightbug.Kinematic2D.Core;
using UnityEngine;

namespace Lightbug.Kinematic2D.Implementation
{
	public class VelocityAreaYouHaveToLandOn : VelocityArea
	{
		public override void Update()
		{
			if (!isActive)
				return;

			if (targetCharacterMotor.Velocity.y < 0f && !targetCharacterMotor.IsGrounded)
			{
				DoAction(targetCharacterMotor);
			}
		}
	}
}