﻿using UnityEngine;

public enum CutsceneActionTypes
{
	Dialogue,
	MoveActor,
	PlaySound
}

public class CutsceneAction
{
	public float start_delay;

	public CutsceneActionTypes type;
	public bool isDone;

	public ConversationObject conversationObject;
	public string soundName;
	public GameObject destination;
}