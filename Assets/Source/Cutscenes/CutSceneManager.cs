﻿using Lightbug.Kinematic2D.Implementation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneManager : MonoBehaviour
{
	List<CutsceneSequence> CutSceneSequences = new List<CutsceneSequence>();

	private CharacterController2D _player;

	GameObject ActorDestination;

	InControlInputController inControlInputController;

	CutsceneSequence CurrentSequence;

	CutsceneAction DialogueAction;
	CutsceneAction PlaySoundAction;
	CutsceneAction MoveActorAction;

	CutsceneSequence SequenceBeingAddedTo;

	void Start()
    {
    }
	
    void Update()
	{
		UpdateSequences();

		if (inControlInputController == null)
		{
			inControlInputController = FindObjectOfType<InControlInputController>();
		}

		if (_player == null)
		{
			GameObject goPlayer = GameObject.FindGameObjectWithTag("Player");

			if (goPlayer != null)
			{
				_player = goPlayer.GetComponent<CharacterController2D>();
			}
		}

		if (ActorDestination != null)
		{
			if (Vector2.Distance(new Vector2(_player.transform.position.x, 0f), new Vector2(ActorDestination.transform.position.x, 0f)) > 0.01f)
			{
				CharacterActionInfo charAct = inControlInputController.CharacterAction;
				if (_player.transform.position.x < ActorDestination.transform.position.x)
				{
					charAct.right = true;
				}
				else if (_player.transform.position.x > ActorDestination.transform.position.x)
				{
					charAct.left = true;
				}
				inControlInputController.SetAction(charAct);
			}
			else
			{
				ActorDestination = null;
				BaseGame.Instance.RecordsService.Player.PlayerHasControlOfCharacter = true;

				MoveActorAction.isDone = true;
				MoveActorAction = null;
			}
		}

		if (PlaySoundAction != null)
		{
			PlaySoundAction.isDone = !BaseGame.Instance.AudioService.IsPlaying(PlaySoundAction.soundName);

			if (PlaySoundAction.isDone)
			{
				PlaySoundAction = null;
			}
		}

		if (DialogueAction != null)
		{
			DialogueAction.isDone = !BaseGame.Instance.DialogueService.IsDialoguePlaying();

			if (DialogueAction.isDone)
			{
				DialogueAction = null;
			}
		}
	}

	void UpdateSequences()
	{
		if (CurrentSequence != null)
		{
			if (CurrentSequence.IsDone())
			{
				CurrentSequence = null;
			}
		}

		if (CutSceneSequences.Count > 0 && CurrentSequence == null)
		{
			CurrentSequence = CutSceneSequences[0];
			CutSceneSequences.RemoveAt(0);

			foreach (CutsceneAction action in CurrentSequence.Actions)
			{
				switch (action.type)
				{
					case CutsceneActionTypes.Dialogue:
						BaseGame.Instance.DialogueService.QueueDialogueEvent(action.conversationObject);
						DialogueAction = action;
						break;
					case CutsceneActionTypes.MoveActor:
						ActorDestination = action.destination;
						BaseGame.Instance.RecordsService.Player.PlayerHasControlOfCharacter = false;
						MoveActorAction = action;
						break;
					case CutsceneActionTypes.PlaySound:
						BaseGame.Instance.AudioService.Play(action.soundName);
						PlaySoundAction = action;
						break;
				}
			}
		}
	}

	public void OpenNewSequence()
	{
		SequenceBeingAddedTo = new CutsceneSequence();
	}

	public void QueueUpDialogueAction(ConversationObject convoObject)
	{
		CutsceneAction action = new CutsceneAction();
		action.type = CutsceneActionTypes.Dialogue;
		action.conversationObject = convoObject;

		QueueAction(action);
	}

	public void QueueUpMoveActorAction(GameObject destination)
	{
		CutsceneAction action = new CutsceneAction();
		action.type = CutsceneActionTypes.MoveActor;
		action.destination = destination;

		QueueAction(action);
	}

	public void QueueUpPlaySoundAction(string soundName)
	{
		CutsceneAction action = new CutsceneAction();
		action.type = CutsceneActionTypes.PlaySound;
		action.soundName = soundName;

		QueueAction(action);
	}

	private void QueueAction(CutsceneAction newAction)
	{
		SequenceBeingAddedTo.AddAction(newAction);
	}

	public void CloseNewSequence()
	{
		CutSceneSequences.Add(SequenceBeingAddedTo);

		SequenceBeingAddedTo = null;
	}
}