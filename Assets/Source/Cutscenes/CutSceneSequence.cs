﻿using System.Collections;
using System.Collections.Generic;

public class CutsceneSequence
{
	public List<CutsceneAction> Actions = new List<CutsceneAction>();

	float end_delay;

	public bool IsDone()
	{
		foreach (CutsceneAction action in Actions)
		{
			if (!action.isDone)
				return false;
		}

		return true;
	}

	public void AddAction(CutsceneAction newAction)
	{
		Actions.Add(newAction);
	}
}