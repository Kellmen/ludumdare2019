﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CutSceneTrigger : MonoBehaviour
{
	public ConversationObject Conversation;
	public int DialoguePlaceInOrder;

	public GameObject Destination;
	public int MoveActorPlaceInOrder;

	public string SoundName;
	public int PlaySoundPlaceInOrder;

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.CompareTag("Player") && (Conversation != null || Destination != null || !string.IsNullOrWhiteSpace(SoundName)))
		{
			for (int i = 0; i < Mathf.Max(DialoguePlaceInOrder, MoveActorPlaceInOrder, PlaySoundPlaceInOrder) + 1; i++)
			{
				BaseGame.Instance.CutsceneManager.OpenNewSequence();

				if (i == DialoguePlaceInOrder)
				{
					if (Conversation != null)
					{
						BaseGame.Instance.CutsceneManager.QueueUpDialogueAction(Conversation);
					}
				}

				if (i == MoveActorPlaceInOrder)
				{
					if (Destination != null)
					{
						BaseGame.Instance.CutsceneManager.QueueUpMoveActorAction(Destination);
					}
				}

				if (i == PlaySoundPlaceInOrder)
				{ 
					if (!string.IsNullOrWhiteSpace(SoundName))
					{
						BaseGame.Instance.CutsceneManager.QueueUpPlaySoundAction(SoundName);
					}
				}

				BaseGame.Instance.CutsceneManager.CloseNewSequence();
			}
		}
	}
}