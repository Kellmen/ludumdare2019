﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ConversationObject : ScriptableObject
{
    [SerializeField]
    private List<DialogueMessage> _dialogueMessages;

    public List<DialogueMessage> DialogueMessages { get { return _dialogueMessages; } }
    public Action OnComplete;
}

[Serializable]
public class DialogueMessage
{
    [SerializeField]
    private SoundInfo _voiceOver;

    [SerializeField]
    private string _message;

    [SerializeField]
    private bool _autoContinue = true;

    [SerializeField]
    private float _startDelay;

    [SerializeField]
    private float _endDelay;

    [SerializeField]
    private List<DialogueChoice> _playerResponseChoices;

    public SoundInfo VoiceOver { get { return _voiceOver; } }
    public List<DialogueChoice> PlayerResponseChoices { get { return _playerResponseChoices; } }
    public string Message { get { return _message; } }
    public bool AutoContinue { get { return _autoContinue; } }
    public float StartDelay { get { return _startDelay; } }
    public float EndDelay { get { return _endDelay; } }
}

[Serializable]
public class DialogueChoice
{
    [SerializeField]
    private string _response;

    [SerializeField]
    private DialogueMessage _followUpMessage;

    public string Response { get { return _response; } }
    public DialogueMessage FollowUpMessage { get { return _followUpMessage; } }
}