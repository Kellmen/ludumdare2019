﻿public interface IDialogueSpeaker
{
    ConversationObject GetConversation();
}