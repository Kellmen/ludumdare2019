﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerDialogueController : MonoBehaviour
{
    private PlayerDeviceRecord _deviceRecord;
    private List<IDialogueSpeaker> _speakerList = new List<IDialogueSpeaker>();

    private void Awake()
    {
        _deviceRecord = BaseGame.Instance.RecordsService.Player.Device;
    }

    private void Update()
    {
        if(_deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_3_WAS_PRESSED))
        {
            HandlePlayerInteractionButtonPressed();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        IDialogueSpeaker speaker = other.GetComponent<IDialogueSpeaker>();

        if (speaker != null)
        {
            _speakerList.Add(speaker);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        IDialogueSpeaker speaker = other.GetComponent<IDialogueSpeaker>();

        if (speaker != null)
        {
            _speakerList.Remove(speaker);
        }
    }

    public void HandlePlayerInteractionButtonPressed()
    {
        if (_speakerList.Count > 0)
        {
            BaseGame.Instance.DialogueService.QueueDialogueEvent(_speakerList[0].GetConversation());
        }
    }
}