﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerResponseButton : MonoBehaviour
{
    [SerializeField]
    private int _buttonIndex;

    [SerializeField]
    private TMPro.TextMeshProUGUI _choiceText;

    [SerializeField]
    private Button _responseButton;

    [SerializeField]
    private GameObject _selectedImage;

    public int ButtonIndex { get { return _buttonIndex; } }
    public TMPro.TextMeshProUGUI ChoiceText { get { return _choiceText; } }
    public Button ResponseButton { get { return _responseButton; } }

    public DialogueMessage FollowUp { get; set; }

    private bool _selected;
    public bool Selected
    {
        get { return _selected; }
        set
        {
            _selected = value;
            _selectedImage.SetActive(_selected);
        }
    }

    void Awake()
    {
        Selected = false;
    }
}
