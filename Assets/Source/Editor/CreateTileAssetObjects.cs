﻿#if UNITY_EDITOR
namespace InControl
{
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.Tilemaps;

    static class CreateTileAssetObjects
    {
        [MenuItem("Create/SleepyHead/CreateTileAssetObjects", false, 100)]
        static void CreateObjects()
        {
            string TileFolderPath = "Assets/Resources/Data/TileAssets/";
            string[] assetGuids = AssetDatabase.FindAssets("", new[] { "Assets/Tiles" });

            foreach(string guid in assetGuids)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                Object TileAsset = AssetDatabase.LoadAssetAtPath(path, typeof(Object));

                Tile tile = TileAsset as Tile;

                TileAssetObject newTileAsset = ScriptableObject.CreateInstance<TileAssetObject>();
                newTileAsset.BlockTile = tile;

                AssetDatabase.CreateAsset(newTileAsset, TileFolderPath + path.Substring(path.LastIndexOf('/') + 1));
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
#endif