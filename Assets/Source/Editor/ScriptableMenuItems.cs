﻿using UnityEditor;

public class YourClassAsset
{
    [MenuItem("Assets/Create/Create Conversation Object")]
    public static void CreateConversationObject()
    {
        ScriptableObjectUtility.CreateAsset<ConversationObject>();
    }

    [MenuItem("Assets/Create/Create PlayingGameStateAssets")]
    public static void CreatePlayingGameStateAssets()
    {
        ScriptableObjectUtility.CreateAsset<PlayingGameStateAssets>();
    }

    [MenuItem("Assets/Create/Create Block Asset")]
    public static void CreateBlockAsset()
    {
        ScriptableObjectUtility.CreateAsset<TilePrefabAssetObject>();
    }
}