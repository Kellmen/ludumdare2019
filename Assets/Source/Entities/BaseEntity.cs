﻿using Lightbug.Kinematic2D.Core;
using Lightbug.Kinematic2D.Implementation;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BaseEntity : MonoBehaviour
{
    [SerializeField]
    protected CharacterController2D _characterController;

    [SerializeField]
    protected CharacterGraphics _characterGraphics;

    [SerializeField]
    protected VerticalMovement _verticalControl;

    [SerializeField]
    protected HorizontalMovement _horizontalControl;

    public Dictionary<string, object> Properties { get; private set; } = new Dictionary<string, object>();

    public event Action<BaseEntity> Destroyed;
    public event Action<BaseEntity> EntityEntered;
    public event Action<BaseEntity> EntityExited;
    public event Action DrawGizmos;

    public Action<int, BaseEntity> TakeDamage;

	public StrategyManager Strategies { get; private set; } = new StrategyManager();
	public Strategy EquippedActionStrategy { get; protected set; }

	public virtual Vector2 Velocity
    {
        get
        {
            _velocity = _characterController.Velocity;
            return _velocity;
        }
        protected set
        {
            _velocity = value;
        }
    }
    private Vector2 _velocity;

    public virtual Vector2 FacingDirection { get; protected set; }

    public CharacterGraphics CharacterGraphics { get { return _characterGraphics; } }
    public CharacterController2D CharacterController { get { return _characterController; } }
    public VerticalMovement VerticalControl { get { return _verticalControl; } }
    public HorizontalMovement HorizontalControl { get { return _horizontalControl; } }

    public BaseEntity ControlledBy { get; protected set; }

    public bool HasControl
    {
        get
        {
            return ControlledBy == null;
        }
    }

    public virtual void Start() { }

    public virtual void OnDestroy()
    {
        Destroyed?.Invoke(this);
    }
	
	public virtual void Update()
	{
		Strategies.Update();
	}

	public void SetProperty<T>(string key, T value)
    {
        Properties[key] = value;
    }

    public bool TryGetProperty<T>(string key, out T value)
    {
        if (Properties.TryGetValue(key, out object val) && val is T)
        {
            value = (T)val;
            return true;
        }

        value = default;
        return false;
    }

    public Vector2 GetPosition()
    {
        return gameObject.transform.position;
    }

    public virtual void SetFacingDirection(Vector2 newFacingDirection, bool forcibly)
    {
        if (HasControl || forcibly)
        {
            FacingDirection = newFacingDirection;

            if (Mathf.Sign(newFacingDirection.x) > 0)
            {
                _characterController.LookToTheRight();
            }
            else
            {
                _characterController.LookToTheLeft();
            }
        }
    }

    public void SetControlledBy(BaseEntity control)
    {
        ControlledBy = control;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        BaseEntity entity = col.GetComponent<BaseEntity>();
        if (entity != null)
        {
            EntityEntered?.Invoke(entity);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        BaseEntity entity = col.GetComponent<BaseEntity>();
        if (entity != null)
        {
            EntityExited?.Invoke(entity);
        }
    }

    private void OnDrawGizmos()
    {
        DrawGizmos?.Invoke();
	}

	//public virtual void Equipped(BaseEntity entity)
	//{
	//	EquippedActionStrategy.OwnerEquipped(entity);
	//}
	//
	//public virtual void Unequipped(BaseEntity entity)
	//{
	//	EquippedActionStrategy.OwnerUnequipped(entity);
	//}

	public virtual void ActivateAbility(BaseEntity entity)
	{
		EquippedActionStrategy.ActivateAbility(entity);
	}

	public virtual void DeactivateAbility(BaseEntity entity)
	{
		EquippedActionStrategy.DeactivateAbility(entity);
	}
}