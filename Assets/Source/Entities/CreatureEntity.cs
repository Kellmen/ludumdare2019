﻿using Lightbug.Kinematic2D.Implementation;
using UnityEngine;

public class CreatureEntity : BaseEntity
{
    [SerializeField]
    private CreatureBrain _brain;

    [SerializeField]
    private CornerAlignment _cornerAlign;

    [SerializeField]
    private WallAlignment _wallAlign;

    [SerializeField]
    private bool _snapToWalls;

    public CreatureBrain Brain { get { return _brain; } }
    public CornerAlignment CornerAlign { get { return _cornerAlign; } }
    public WallAlignment WallAlign { get { return _wallAlign; } }
    public bool SnapToWalls { get { return _snapToWalls; } }
}