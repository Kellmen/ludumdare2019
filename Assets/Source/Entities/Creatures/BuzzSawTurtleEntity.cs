﻿using UnityEngine;

public class BuzzSawTurtleEntity : CreatureEntity
{
	public StrategyManager Strategies { get; private set; } = new StrategyManager();
	public Resource Health { get; private set; } = new Resource(2);

	public override void Start()
	{
		base.Start();

		Strategies.Add(new DamageEntityOnTouchStrategy(this, 1));
        Strategies.Add(new EntityAIWalkStrategy(this, true));
        Strategies.Add(new TurnAroundAtObstaclesStrategy(this, new Vector2(0.325f, 0.15f)));
    }

    public override void Update()
	{
		Strategies.Update();
	}
}