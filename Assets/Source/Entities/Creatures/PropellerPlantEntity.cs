﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropellerPlantEntity : CreatureEntity
{
	public Resource Health { get; private set; } = new Resource(2);

	private IBaseGame _baseGame;

	public override void Start()
	{
		base.Start();

		FacingDirection = Vector2.right;

		EquippedActionStrategy = new PropellerStrategy(this);

		Strategies.Add(EquippedActionStrategy);
	}

	public override void Update()
	{
		base.Update();
	}
}
