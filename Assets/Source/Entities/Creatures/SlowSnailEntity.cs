﻿using UnityEngine;

public class SlowSnailEntity : CreatureEntity
{
	public StrategyManager Strategies { get; private set; } = new StrategyManager();
	public Resource Health { get; private set; } = new Resource(2);

	private IBaseGame _baseGame;

	public override void Start()
	{
		base.Start();
		
		Strategies.Add(new EntityAIWalkStrategy(this, true));
        Strategies.Add(new TurnAroundAtObstaclesStrategy(this, new Vector2(0.325f, 0.15f)));
		Strategies.Add(new ActAsWallWhenPushedAgainstAWallStrategy(this, new Vector2(0.325f, 0.15f)));
	}

    public override void Update()
	{
		Strategies.Update();
	}
}