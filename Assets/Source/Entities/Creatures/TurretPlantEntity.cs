﻿using UnityEngine;

public class TurretPlantEntity : CreatureEntity
{
	[SerializeField]
	GameObject _projectilePrefab;

    public Resource Health { get; private set; } = new Resource(2);

    private IBaseGame _baseGame;

    public override void Start()
	{
        base.Start();

		FacingDirection = Vector2.right;

		_baseGame = BaseGame.Instance;
        _baseGame.CommonGameElementsService.TryGetCommonElement("player", out PlayerEntity player);

		EquippedActionStrategy = new ShootProjectileAtEntityStrategy(this, player, 1, _projectilePrefab);

		Strategies.Add(EquippedActionStrategy);
	}

    public override void Update()
    {
        Strategies.Update();
	}

	public override void ActivateAbility(BaseEntity entity)
	{
		EquippedActionStrategy.ActivateAbility(entity);
	}
}