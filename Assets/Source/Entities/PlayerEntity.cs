﻿using Lightbug.Kinematic2D.Core;
using UnityEngine;

public class PlayerEntity : BaseEntity
{
    [SerializeField]
    private Transform _vacuumAnchor;

    [SerializeField]
    private InControlInputController _inputController;
	
    public InControlInputController InputController { get { return _inputController; } }

	private VacuumCreatureStrategy vacuumCreatureStrategy;
	private UseEquippedEntityStrategy useEquippedEntityStrategy;

	BaseEntity vacuumEntity;

	public override Vector2 FacingDirection
    {
        get
        {
            _facingDirection = _characterController.IsFacingRight ? Vector2.right : Vector2.left;
            return _facingDirection;
        }
        protected set
        {
            _facingDirection = value;
        }
    }
    private Vector2 _facingDirection;

    public override void Start()
    {
        base.Start();

		vacuumCreatureStrategy = new VacuumCreatureStrategy(this, _inputController, new Vector2(0, _characterController.CharacterBody.height / 2), _vacuumAnchor, 3f, 1f);
		useEquippedEntityStrategy = new UseEquippedEntityStrategy(this, _inputController, new Vector2(0, _characterController.CharacterBody.height / 2), _vacuumAnchor);

		Strategies.Add(vacuumCreatureStrategy);
		Strategies.Add(useEquippedEntityStrategy);
	}

    public override void Update()
    {
        Strategies.Update();

		Strategy equippedStrategy = useEquippedEntityStrategy.GetEquippedStrategy();
		if (equippedStrategy != null)
		{
			equippedStrategy.Update();
		}
	}

	public void SetEquippedEntityStrategy(Strategy newEquippedStrategy)
	{
		useEquippedEntityStrategy.SetEquippedStrategy(newEquippedStrategy);
	}


	public override void SetFacingDirection(Vector2 newFacingDirection, bool forcibly)
    {
    }
}