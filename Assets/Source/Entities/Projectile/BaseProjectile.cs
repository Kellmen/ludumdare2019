﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : PoolableObject, IProjectile
{
	[SerializeField]
	protected float _speed;

	[SerializeField]
	protected int _damage;

	[SerializeField]
	private float _gravity;
	private float _currentGravity = 0f;

	[SerializeField]
	private Vector2 _knockBack;

	[SerializeField]
	private int _blastRadius;

	[SerializeField]
	private float _blastKnockBackFallOff;

	private Rigidbody2D _rigidbody2D;

	private List<BaseEntity> EntitiesInTrigger = new List<BaseEntity>();

	[SerializeField]
	private int _numberOfHitsBeforeDying;
	private int _numberOfHitsDealt = 0;

	[SerializeField]
	private int _damageToDealBeforeDying;
	private int _damageDealt = 0;

	[SerializeField]
	private float _durationActiveBeforeDying;
	private float timeOfFiring = 0f;

	[SerializeField]
	private float _distanceTraveledBeforeDying;
	private Vector2 _spawnPosition = Vector2.zero;

	private bool _isActive = false;

	public virtual void Awake()
	{
		_rigidbody2D = GetComponent<Rigidbody2D>();
	}

	public virtual void Start()
	{
	}

	public virtual void Update()
	{
		if (_numberOfHitsDealt >= _numberOfHitsBeforeDying)
		{
			Die();
		}

		if (_damageDealt >= _damageToDealBeforeDying)
		{
			Die();
		}

		if (Time.timeSinceLevelLoad - timeOfFiring >= _durationActiveBeforeDying)
		{
			Die();
		}

		if (Vector2.Distance(_spawnPosition, transform.position) >= _distanceTraveledBeforeDying)
		{
			Die();
		}

		if (_gravity != 0f)
		{
			_currentGravity += _gravity;
			_rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, _rigidbody2D.velocity.y - _currentGravity);
		}
	}

	public virtual void Fire(Vector2 spawnPosition, Vector2 initialVelocity)
	{
		transform.position = _spawnPosition = spawnPosition;
		_rigidbody2D.velocity = initialVelocity;

		timeOfFiring = Time.timeSinceLevelLoad;

		_numberOfHitsDealt = 0;
		_damageDealt = 0;
	}

	public virtual void IsActive()
	{
	}

	public virtual void IsDying()
	{
	}

	public virtual void Die()
	{
		BaseGame.Instance.ObjectPool.AddObjectToPool(this);
		//Destroy(gameObject);
	}

	public virtual void Hit(BaseEntity hitEntity)
	{
		hitEntity.TakeDamage?.Invoke(_damage, null);

		_damageDealt += _damage;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
        BaseEntity entity = col.GetComponent<BaseEntity>();
		if (entity != null)
		{
			if (!EntitiesInTrigger.Contains(entity))
			{
				EntitiesInTrigger.Add(entity);
				Hit(entity);
			}
		}
		else if (col.CompareTag("ElementSquare"))
		{
			Die();
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
        BaseEntity entity = col.GetComponent<BaseEntity>();
		if (entity != null)
		{
			EntitiesInTrigger.Remove(entity);
		}
	}

	public float GetSpeed()
	{
		return _speed;
	}
}
