﻿using UnityEngine;

public interface IProjectile
{
	void Awake();
	void Start();
	void Update();
	void Fire(Vector2 spawnPosition, Vector2 initialVelocity);
	void IsActive();
	void IsDying();
	void Hit(BaseEntity hitEntity);
	void Die();
	float GetSpeed();
}
