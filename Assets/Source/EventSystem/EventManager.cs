﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class EventManager : MonoBehaviour 
{
	private Dictionary <string, UnityGameEvent> eventDictionary;

	private static EventManager eventManager;

	public static EventManager instance
	{
		get
		{
			if (!eventManager)
			{
				eventManager = FindObjectOfType (typeof (EventManager)) as EventManager;

				if (!eventManager)
				{
					Debug.LogError ("There needs to be one active EventManger script on a GameObject in your scene.");
				}
				else
				{
					eventManager.Init ();
				}
			}

			return eventManager;
		}
	}

	void Init ()
	{
		if (eventDictionary == null)
		{
			eventDictionary = new Dictionary<string, UnityGameEvent>();
		}
	}

	public static void AddListener (string eventName, UnityAction<GameEvent> listener)
	{
		UnityGameEvent thisEvent = null;
		if (instance.eventDictionary.TryGetValue (eventName, out thisEvent))
		{
			thisEvent.AddListener (listener);
		} 
		else
		{
			thisEvent = new UnityGameEvent();
			thisEvent.AddListener (listener);
			instance.eventDictionary.Add (eventName, thisEvent);
		}
	}

	public static void RemoveListener (string eventName, UnityAction<GameEvent> listener)
	{
		if (eventManager == null) return;

		UnityGameEvent thisEvent = null;
		if (instance.eventDictionary.TryGetValue (eventName, out thisEvent))
		{
			thisEvent.RemoveListener (listener);
		}
	}

	public static void Broadcast (GameEvent evt)
	{
		UnityGameEvent thisEvent = null;
		if (instance.eventDictionary.TryGetValue (evt.eventName, out thisEvent))
		{
			thisEvent.Invoke (evt);
		}
	}

	public static bool HasEventBeenRegistered(string eventName)
	{
		return instance.eventDictionary.ContainsKey(eventName);
	}

	private class UnityGameEvent : UnityEvent<GameEvent> {};
}