﻿using UnityEngine;

public sealed class BaseGame : MonoBehaviour, IBaseGame
{
    public static IBaseGame Instance { get; private set; }

    [SerializeField]
    private BaseGameView _view;

    private GameStateController _gameStateController = new GameStateController();

    private IRecordsService _recordsService;
    private IGameAudioService _audioService;
    private IDialogueService _dialogueService;
    private ICommonGameElementsService _commonGameElementsService;

    private CutSceneManager _cutSceneManager;
	private ObjectPool _objectPool;

	private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(this.gameObject);

        _recordsService = new RecordsService(this);
        _audioService = new GameAudioService(this, _view.AudioView);
        _dialogueService = new DialogueService(this, _view.DialogueServiceView);
        _commonGameElementsService = new CommonGameElementsService();

        _recordsService.InitializeRecords();

        // TESTING
        _recordsService.Player.DebugPlayerLevel = _view.DebugPlayerLevel;
        //

        _gameStateController.SwitchState(new MainMenuGameState());

        _cutSceneManager = _view.CutSceneManager;
		_objectPool = _view.ObjectPool;
	}

    public void Update()
    {
        _gameStateController.Update();
    }

    void IBaseGame.SwitchState(IGameState state)
    {
        _gameStateController.SwitchState(state);
    }

    IRecordsService IBaseGame.RecordsService { get { return _recordsService; } }
    IGameAudioService IBaseGame.AudioService { get { return _audioService; } }
    IDialogueService IBaseGame.DialogueService { get { return _dialogueService; } }
    ICommonGameElementsService IBaseGame.CommonGameElementsService { get { return _commonGameElementsService; } }
    CutSceneManager IBaseGame.CutsceneManager { get { return _cutSceneManager; } }
    Transform IBaseGame.ViewRoot { get { return _view.ViewRoot; } }
	ObjectPool IBaseGame.ObjectPool { get { return _objectPool; } }
}