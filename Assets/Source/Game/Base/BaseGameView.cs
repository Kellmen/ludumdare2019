﻿using UnityEngine;

public sealed class BaseGameView : MonoBehaviour
{
    [Header("DEBUG")]
    public string DebugPlayerLevel;

    [SerializeField]
    private Transform _viewRoot;

    [SerializeField]
    private GameAudioServiceView _audioView;

    [SerializeField]
    private DialogueServiceView _dialogueServiceView;

	[SerializeField]
	private CutSceneManager _cutSceneManager;

	[SerializeField]
	private ObjectPool _objectPool;

	public Transform ViewRoot { get { return _viewRoot; } }
    public GameAudioServiceView AudioView { get { return _audioView; } }
    public DialogueServiceView DialogueServiceView { get { return _dialogueServiceView; } }
	public CutSceneManager CutSceneManager { get { return _cutSceneManager; } }
	public ObjectPool ObjectPool { get { return _objectPool; } }
}