﻿using UnityEngine;

public interface IBaseGame
{
    Transform ViewRoot { get; }

    IRecordsService RecordsService { get; }
    IGameAudioService AudioService { get; }
    IDialogueService DialogueService { get; }
    ICommonGameElementsService CommonGameElementsService { get; }
    CutSceneManager CutsceneManager { get; }
	ObjectPool ObjectPool { get; }

	void SwitchState(IGameState state);
}