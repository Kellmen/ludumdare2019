﻿using System.Collections;
using UnityEngine;

public class BurnableBlock : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _burningParticles;

    public void Burn()
    {
        _burningParticles.gameObject.SetActive(true);
        StartCoroutine(DoBurn());
    }

    private IEnumerator DoBurn()
    {
        yield return new WaitForSeconds(2.0f);

        Vector2 pos = new Vector2(transform.position.x, transform.position.y + 0.5f);

        RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
        if(hit.collider != null && hit.collider.gameObject.CompareTag("Burnable"))
        {
            BurnableBlock burnBlock = hit.collider.gameObject.GetComponent<BurnableBlock>();

            if (burnBlock != null)
            {
                burnBlock.Burn();
            }
        }

        pos = new Vector2(transform.position.x + 0.5f, transform.position.y);

        hit = Physics2D.Raycast(pos, Vector2.zero);
        if (hit.collider != null && hit.collider.gameObject.CompareTag("Burnable"))
        {
            BurnableBlock burnBlock = hit.collider.gameObject.GetComponent<BurnableBlock>();

            if (burnBlock != null)
            {
                burnBlock.Burn();
            }
        }

        pos = new Vector2(transform.position.x - 0.5f, transform.position.y);

        hit = Physics2D.Raycast(pos, Vector2.zero);
        if (hit.collider != null && hit.collider.gameObject.CompareTag("Burnable"))
        {
            BurnableBlock burnBlock = hit.collider.gameObject.GetComponent<BurnableBlock>();

            if (burnBlock != null)
            {
                burnBlock.Burn();
            }
        }

        pos = new Vector2(transform.position.x, transform.position.y - 0.5f);

        hit = Physics2D.Raycast(pos, Vector2.zero);
        if (hit.collider != null && hit.collider.gameObject.CompareTag("Burnable"))
        {
            BurnableBlock burnBlock = hit.collider.gameObject.GetComponent<BurnableBlock>();

            if (burnBlock != null)
            {
                burnBlock.Burn();
            }
        }

        this.gameObject.SetActive(false);
    }

    public void Reset()
    {
        StopAllCoroutines();
        _burningParticles.Stop();
        _burningParticles.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}