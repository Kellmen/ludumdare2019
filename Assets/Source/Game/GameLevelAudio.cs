﻿using System.Collections.Generic;
using UnityEngine;

public class GameLevelAudio : MonoBehaviour
{
    [SerializeField]
    private List<SoundInfo> _musicTracks;

    [SerializeField]
    private List<SoundInfo> _goalSounds;

    [SerializeField]
    private List<SoundInfo> _deathVoiceOvers;

    [SerializeField]
    private List<SoundInfo> _deathSounds;

    [SerializeField]
    private SoundInfo _gameCompleteSound;

    [SerializeField]
    private List<SoundInfo> _blockPlacedSounds;

    [SerializeField]
    private SoundInfo _goalCollectSound;

    private IBaseGame _baseGame;

    private int _lastDeathSoundIndex = -1;
    private int _lastGoalSoundIndex = -1;
    private int _lastDeathVoiceOverSoundIndex = -1;

    void Awake()
    {
        _baseGame = BaseGame.Instance;
        _baseGame.AudioService.Add(_gameCompleteSound);
        _baseGame.AudioService.Add(_goalCollectSound);

        foreach (SoundInfo sound in _blockPlacedSounds)
        {
            _baseGame.AudioService.Add(sound);
        }

        foreach (SoundInfo sound in _goalSounds)
        {
            _baseGame.AudioService.Add(sound);
        }

        foreach (SoundInfo sound in _musicTracks)
        {
            _baseGame.AudioService.Add(sound);
        }

        foreach (SoundInfo sound in _deathVoiceOvers)
        {
            _baseGame.AudioService.Add(sound);
        }

        foreach (SoundInfo sound in _deathSounds)
        {
            _baseGame.AudioService.Add(sound);
        }
    }

    public void InitMusicTracks()
    {
        for (int i = 0; i < _musicTracks.Count; ++i)
        {
            _baseGame.AudioService.SetTrackVolume(_musicTracks[i].name, i == 0 ? 0.2f : 0);
            _baseGame.AudioService.Play(_musicTracks[i].name);
        }
    }

    public void PlayElementSound(string type)
    {
        //_baseGame.AudioService.Play(_blockPlacedSounds[(int)element - 1].name);
    }

    public void PlayGoalCollectSound()
    {
        _baseGame.AudioService.Play(_goalCollectSound.name);
    }

    public void PlayGoalSound()
    {
        int soundIndex = Random.Range(0, _goalSounds.Count);

        while (soundIndex == _lastGoalSoundIndex)
        {
            soundIndex = Random.Range(0, _goalSounds.Count);
        }

        _baseGame.AudioService.Play(_goalSounds[soundIndex].name);
        _lastGoalSoundIndex = soundIndex;
    }

    public void PlayDeathSound(bool doVoiceOver)
    {
        int soundIndex = Random.Range(0, _deathSounds.Count);

        while (soundIndex == _lastDeathSoundIndex)
        {
            soundIndex = Random.Range(0, _deathSounds.Count);
        }

        _baseGame.AudioService.Play(_deathSounds[soundIndex].name);
        _lastDeathSoundIndex = soundIndex;

        if (doVoiceOver)
        {
            soundIndex = Random.Range(0, _deathVoiceOvers.Count);

            while (soundIndex == _lastDeathVoiceOverSoundIndex)
            {
                soundIndex = Random.Range(0, _deathVoiceOvers.Count);
            }

            _baseGame.AudioService.Play(_deathVoiceOvers[soundIndex].name);
            _lastDeathVoiceOverSoundIndex = soundIndex;
        }
    }

    public void PlayGameComplete()
    {
        _baseGame.AudioService.Play(_gameCompleteSound.name);
    }
}