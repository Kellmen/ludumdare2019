﻿using Lightbug.Kinematic2D.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridManager : MonoBehaviour
{
    [SerializeField]
    private GameView _gameView;

    [SerializeField]
    private GameLevelAudio _gameLevelAudio;

    [SerializeField]
    private List<GameObject> _placeableBlocks;

    [SerializeField]
    private Grid _gameGrid;

    private IBaseGame _baseGame;

    [SerializeField]
    private MouseFollowingTileGraphic _mouseFollowingElement;

    [SerializeField]
    private List<Tilemap> _tileMaps;

    private BurnableBlock[] _mapBurnableBlocks = new BurnableBlock[0];

    private Vector3 _gridCellToWorldPosition;
    private Vector3 _mouseWorldPos;
    private Vector3Int _mousePosCell;

    public Grid GameGrid { get { return _gameGrid; } }

    public MouseFollowingTileGraphic MouseFollowingElement { get { return _mouseFollowingElement; } }
    public List<string> TileMapLayerNames { get; private set; }
    public Dictionary<string, ITileAsset> PrefabTileAssetObjects { get; private set; }

    public int CurrentTileMap { get; private set; }

    public event Action OnTileMapChanged;

    public List<GridTile> GridTiles { get; private set; }

    private void Awake()
    {
        _baseGame = BaseGame.Instance;

        TileMapLayerNames = new List<string>();
        foreach (Tilemap tilemap in _tileMaps)
        {
            TileMapLayerNames.Add(tilemap.gameObject.name);
        }

        // There should always be a tile map named "Level"
        CurrentTileMap = TileMapLayerNames.IndexOf("Level") >= 0 ? TileMapLayerNames.IndexOf("Level") : 0;

        GridTiles = new List<GridTile>();

        PrefabTileAssetObjects = new Dictionary<string, ITileAsset>();
        UnityEngine.Object[] blockAssets = Resources.LoadAll("Data/PrefabTileAssets", typeof(TilePrefabAssetObject));
        UnityEngine.Object[] tileAssets = Resources.LoadAll("Data/TileAssets", typeof(TileAssetObject));

        foreach (UnityEngine.Object obj in blockAssets)
        {
            TilePrefabAssetObject blockAssetObject = obj as TilePrefabAssetObject;

            PrefabTileAssetObjects.Add(blockAssetObject.Name, blockAssetObject);
            _mouseFollowingElement.AddTileObject(blockAssetObject);
        }

        foreach (UnityEngine.Object obj in tileAssets)
        {
            TileAssetObject tileAssetObject = obj as TileAssetObject;

            PrefabTileAssetObjects.Add(tileAssetObject.Name, tileAssetObject);
        }
    }

    private void Update()
    {
        if (_gameView.GameCamera.gameObject.activeInHierarchy)
        {
            _mouseWorldPos = _gameView.GameCamera.GameCamera.ScreenToWorldPoint(Input.mousePosition);
        }
        else if (_gameView.LevelEditorCamera.gameObject.activeInHierarchy)
        {
            _mouseWorldPos = _gameView.LevelEditorCamera.GameCamera.ScreenToWorldPoint(Input.mousePosition);
        }

        _mousePosCell = GameGrid.WorldToCell(_mouseWorldPos);
        _gridCellToWorldPosition = GameGrid.CellToWorld(_mousePosCell);

        MouseFollowingElement.UpdatePosition(new Vector3(_gridCellToWorldPosition.x + GameGrid.cellSize.x / 2f, _gridCellToWorldPosition.y + GameGrid.cellSize.y / 2f, 0f));
    }

    public void PlaceTile(ITileAsset blockAsset, Vector3Int coordinate)
    {
        if (GetGridTile(coordinate) == null)
        {
            if (blockAsset is TilePrefabAssetObject)
            {
                TilePrefabAssetObject prefabObj = blockAsset as TilePrefabAssetObject;
                GameObject newTile = Instantiate(prefabObj.BlockPrefab);
                Vector3 gridCellToWorldPosition = _gameGrid.CellToWorld(coordinate);
                Vector3 newBlockPosition = new Vector3(gridCellToWorldPosition.x + _gameGrid.cellSize.x / 2f, gridCellToWorldPosition.y + _gameGrid.cellSize.y / 2f, 0f);

                CharacterMotor motor = newTile.GetComponent<CharacterMotor>();

                if (motor != null)
                {
                    motor.Teleport(newBlockPosition + newTile.transform.localPosition);
                }
                else
                {
                    newTile.transform.position = newBlockPosition + newTile.transform.localPosition;
                }

                GridTiles.Add(new GridTile(blockAsset.Name, newTile, coordinate));
            }
            else if (blockAsset is TileAssetObject)
            {
                TileAssetObject tileAsset = blockAsset as TileAssetObject;
                _tileMaps[CurrentTileMap].SetTile(coordinate, tileAsset.BlockTile);

                GridTiles.Add(new GridTile(blockAsset.Name, null, coordinate));
            }
        }
    }

    public void RemoveTile(Vector3Int Coordinate)
    {
        GridTile toRemove = GetGridTile(Coordinate);

        _tileMaps[CurrentTileMap].SetTile(Coordinate, null);

        if (toRemove != null)
        {
            if (toRemove.Tile != null)
            {
                BaseEntity entity = toRemove.Tile.GetComponent<BaseEntity>();

                if (entity != null)
                {
                    GameObject.Destroy(entity.CharacterGraphics.gameObject);
                }

                GameObject.Destroy(toRemove.Tile);
            }

            GridTiles.Remove(toRemove);
        }
    }

    public GridTile GetGridTile(Vector3Int coordinate)
    {
        return GridTiles.Find((GridTile gridTile) => gridTile.Coordinate == coordinate);
    }

    public void ActivateTileMap(Action OnComplete)
    {
        StartCoroutine(DoActivateTilemap(OnComplete));
    }

    public void DeActivateTileMap(Action OnComplete, float interval)
    {
        StartCoroutine(DoDeActivateTilemap(OnComplete, interval));
    }

    private IEnumerator DoDeActivateTilemap(Action OnComplete, float interval)
    {
        if (_tileMaps[CurrentTileMap] != null)
        {
            if (interval < 1)
            {
                while (_tileMaps[CurrentTileMap].color.a > 0)
                {
                    _tileMaps[CurrentTileMap].color = new Color(_tileMaps[CurrentTileMap].color.r, _tileMaps[CurrentTileMap].color.g, _tileMaps[CurrentTileMap].color.b, _tileMaps[CurrentTileMap].color.a - interval > 0 ? _tileMaps[CurrentTileMap].color.a - interval : 0);

                    yield return new WaitForSeconds(0.01f);
                }
            }

            _tileMaps[CurrentTileMap].color = new Color(_tileMaps[CurrentTileMap].color.r, _tileMaps[CurrentTileMap].color.g, _tileMaps[CurrentTileMap].color.b, 0.0f);
            _tileMaps[CurrentTileMap].gameObject.SetActive(false);
        }

        OnComplete?.Invoke();
    }

    private IEnumerator DoActivateTilemap(Action OnComplete)
    {
        float alphaInterval = 0.02f;

        _tileMaps[CurrentTileMap].color = new Color(_tileMaps[CurrentTileMap].color.r, _tileMaps[CurrentTileMap].color.g, _tileMaps[CurrentTileMap].color.b, 0.0f);
        _tileMaps[CurrentTileMap].gameObject.SetActive(true);

        if (_tileMaps[CurrentTileMap] != null)
        {
            while (_tileMaps[CurrentTileMap].color.a < 1)
            {
                _tileMaps[CurrentTileMap].color = new Color(_tileMaps[CurrentTileMap].color.r, _tileMaps[CurrentTileMap].color.g, _tileMaps[CurrentTileMap].color.b, _tileMaps[CurrentTileMap].color.a + alphaInterval >= 1 ? _tileMaps[CurrentTileMap].color.a + alphaInterval : 1);

                yield return new WaitForSeconds(0.01f);
            }
        }

        _mapBurnableBlocks = _tileMaps[CurrentTileMap].gameObject.GetComponentsInChildren<BurnableBlock>();

        OnComplete?.Invoke();
    }

    public TileBase GetCurrentTileMapTile(Vector3Int coordinate)
    {
        return _tileMaps[CurrentTileMap].GetTile(coordinate);
    }

    public void SetActiveTileMap(string mapName)
    {
        int index = TileMapLayerNames.IndexOf(mapName);
        if (index >= 0)
        {
            CurrentTileMap = index;

            OnTileMapChanged?.Invoke();
        }
    }

    public void SetTileMapAlpha(string mapName, float alpha)
    {
        int index = TileMapLayerNames.IndexOf(mapName);
        if (index >= 0)
        {
            _tileMaps[index].color = new Color(_tileMaps[index].color.r, _tileMaps[index].color.g, _tileMaps[index].color.b, alpha);
        }
    }

    public void ResetTileMapAlpha()
    {
        foreach (Tilemap tileMap in _tileMaps)
        {
            tileMap.color = new Color(tileMap.color.r, tileMap.color.g, tileMap.color.b, 1.0f);
        }
    }

    public void ResetDestroyableTiles()
    {
        for (int i = 0; i < _mapBurnableBlocks.Length; ++i)
        {
            _mapBurnableBlocks[i].gameObject.SetActive(true);
            _mapBurnableBlocks[i].Reset();
        }
    }

    public void ClearAllTiles()
    {
        for (int i = GridTiles.Count - 1; i >= 0; i--)
        {
            if (GridTiles[i].Tile != null)
            {
                BaseEntity entity = GridTiles[i].Tile.GetComponent<BaseEntity>();

                if (entity != null)
                {
                    GameObject.Destroy(entity.CharacterGraphics.gameObject);
                }

                GameObject.Destroy(GridTiles[i].Tile);
            }
        }

        foreach (Tilemap tilemap in _tileMaps)
        {
            tilemap.ClearAllTiles();
        }

        GridTiles.Clear();
        _mapBurnableBlocks = new BurnableBlock[0];
    }
}

public class GridTile
{
    public string AssetObjectName { get; private set; }
    public GameObject Tile { get; private set; }
    public Vector3Int Coordinate { get; set; }
    public bool IsMoving = false;

    public GridTile(string assetObjectName, GameObject tile, Vector3Int coordinate)
    {
        AssetObjectName = assetObjectName;
        Tile = tile;
        Coordinate = coordinate;
    }
}