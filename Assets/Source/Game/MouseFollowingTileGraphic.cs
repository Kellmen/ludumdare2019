﻿using System.Collections.Generic;
using UnityEngine;

public class MouseFollowingTileGraphic : MonoBehaviour
{
    [SerializeField]
    private Transform _parentTransform;

    [SerializeField]
    private SpriteRenderer _spriteGraphic;

    [SerializeField]
    private Sprite _eraserSprite;

    public ITileAsset CurrentTileType { get; private set; }

    private Dictionary<string, GameObject> _mouseObjects = new Dictionary<string, GameObject>();

    public void AddTileObject(TilePrefabAssetObject assetObject)
    {
        _mouseObjects.Add(assetObject.Name, Instantiate(assetObject.MouseFollowObject, _parentTransform, false));
    }

    public void UpdatePosition(Vector3 position)
    {
        transform.position = new Vector3(position.x, position.y, transform.position.z);
    }

    public void SetTileType(ITileAsset tileAsset)
    {
        if (tileAsset == null)
            return;

        if (tileAsset is TilePrefabAssetObject)
        {
            bool tileFound = false;
            foreach (KeyValuePair<string, GameObject> obj in _mouseObjects)
            {
                if (obj.Key == tileAsset.Name)
                {
                    obj.Value.SetActive(true);
                    CurrentTileType = tileAsset;
                    tileFound = true;
                }
                else
                {
                    obj.Value.SetActive(false);
                }
            }

            if (tileFound)
            {
                _spriteGraphic.gameObject.SetActive(false);
            }
        }
        else if (tileAsset is TileAssetObject)
        {
            _spriteGraphic.sprite = tileAsset.BlockSprite;
            _spriteGraphic.gameObject.SetActive(true);

            CurrentTileType = tileAsset;
        }
    }

    public void ShowEraser()
    {
        _spriteGraphic.sprite = _eraserSprite;
        _spriteGraphic.gameObject.SetActive(true);

        foreach (KeyValuePair<string, GameObject> obj in _mouseObjects)
        {
            obj.Value.SetActive(false);
        }

        CurrentTileType = null;
    }

    public void TurnOff()
    {
        foreach (KeyValuePair<string, GameObject> obj in _mouseObjects)
        {
            obj.Value.SetActive(false);
        }

        CurrentTileType = null;

        _spriteGraphic.gameObject.SetActive(false);
    }
}