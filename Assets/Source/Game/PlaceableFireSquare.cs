﻿using System.Collections;
using UnityEngine;

public class PlaceableFireSquare : MonoBehaviour
{
    private void Start()
    {
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, Vector2.zero);

        if (hit.collider != null && hit.collider.CompareTag("Burnable"))
        {
            BurnableBlock burnBlock = hit.collider.gameObject.GetComponent<BurnableBlock>();

            if (burnBlock != null)
            {
                burnBlock.Burn();
            }
        }

        StartCoroutine(DoBurn());
    }

    private IEnumerator DoBurn()
    {
        yield return new WaitForSeconds(2.0f);

        GameObject.Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}