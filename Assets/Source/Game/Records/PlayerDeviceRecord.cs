﻿using UnityEngine;
using InControl;

public enum ButtonActionTypes
{
    COMMAND_BUTTON_WAS_PRESSED,
    COMMAND_BUTTON_IS_PRESSED,
    COMMAND_BUTTON_WAS_RELEASED,

    ACTION_BUTTON_1_WAS_PRESSED,
    ACTION_BUTTON_1_IS_PRESSED,
    ACTION_BUTTON_1_WAS_RELEASED,

    ACTION_BUTTON_2_WAS_PRESSED,
    ACTION_BUTTON_2_IS_PRESSED,
    ACTION_BUTTON_2_WAS_RELEASED,

    ACTION_BUTTON_3_WAS_PRESSED,
    ACTION_BUTTON_3_IS_PRESSED,
    ACTION_BUTTON_3_WAS_RELEASED,

    ACTION_BUTTON_4_WAS_PRESSED,
    ACTION_BUTTON_4_IS_PRESSED,
    ACTION_BUTTON_4_WAS_RELEASED,

    MOVE_DIRECTION_RIGHT,
    MOVE_DIRECTION_LEFT,
    MOVE_DIRECTION_UP,
    MOVE_DIRECTION_DOWN,

    COUNT
};

public class PlayerDeviceRecord
{
    public const float STICK_HORIZ_THRESHOLD = 0.4f;
    public const float STICK_VERT_THRESHOLD = 0.8f;

    private InputDevice _device;
    private ButtonActionHandler[] _buttonActionHandlers = new ButtonActionHandler[(int)ButtonActionTypes.COUNT];

    public delegate bool ButtonActionCheck();

    public bool UsingKeyboard { get; private set; }

    public void SetDevice(InputDevice device)
    {
        _device = device;
        UsingKeyboard = _device == null;

        _buttonActionHandlers[(int)ButtonActionTypes.COMMAND_BUTTON_WAS_PRESSED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.CommandWasPressed : false, () => Input.GetKeyDown(KeyCode.Return));
        _buttonActionHandlers[(int)ButtonActionTypes.COMMAND_BUTTON_IS_PRESSED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.CommandIsPressed : false, () => Input.GetKey(KeyCode.Return));
        _buttonActionHandlers[(int)ButtonActionTypes.COMMAND_BUTTON_WAS_RELEASED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.CommandWasReleased : false, () => !Input.GetKeyUp(KeyCode.Return));

        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_1_WAS_PRESSED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action1.WasPressed : false, () => Input.GetKeyDown(KeyCode.Space));
        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_1_IS_PRESSED] = new ButtonActionHandler(() => !UsingKeyboard ? device.Action1.IsPressed : false, () => Input.GetKey(KeyCode.Space));
        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_1_WAS_RELEASED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action1.WasReleased : false, () => Input.GetKeyUp(KeyCode.Space));

        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_2_WAS_PRESSED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action2.WasPressed : false, () => Input.GetKeyDown(KeyCode.P));
        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_2_IS_PRESSED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action2.IsPressed : false, () => Input.GetKey(KeyCode.P));
        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_2_WAS_RELEASED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action2.WasReleased : false, () => Input.GetKeyUp(KeyCode.P));

        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_3_WAS_PRESSED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action3.WasPressed : false, () => Input.GetKeyDown(KeyCode.O));
        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_3_IS_PRESSED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action3.IsPressed : false, () => Input.GetKey(KeyCode.O));
        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_3_WAS_RELEASED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action3.WasReleased : false, () => Input.GetKeyUp(KeyCode.O));

        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_4_WAS_PRESSED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action4.WasPressed : false, () => Input.GetKeyDown(KeyCode.L));
        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_4_IS_PRESSED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action4.IsPressed : false, () => Input.GetKey(KeyCode.L));
        _buttonActionHandlers[(int)ButtonActionTypes.ACTION_BUTTON_4_WAS_RELEASED] = new ButtonActionHandler(() => !UsingKeyboard ? _device.Action4.WasReleased : false, () => Input.GetKeyUp(KeyCode.L));


        _buttonActionHandlers[(int)ButtonActionTypes.MOVE_DIRECTION_RIGHT] = new ButtonActionHandler(() => !UsingKeyboard ? _device.LeftStickX > PlayerDeviceRecord.STICK_HORIZ_THRESHOLD : false, () => Input.GetAxis("Horizontal") > PlayerDeviceRecord.STICK_HORIZ_THRESHOLD);
        _buttonActionHandlers[(int)ButtonActionTypes.MOVE_DIRECTION_LEFT] = new ButtonActionHandler(() => !UsingKeyboard ? _device.LeftStickX < -PlayerDeviceRecord.STICK_HORIZ_THRESHOLD : false, () => Input.GetAxis("Horizontal") < -PlayerDeviceRecord.STICK_HORIZ_THRESHOLD);
        _buttonActionHandlers[(int)ButtonActionTypes.MOVE_DIRECTION_UP] = new ButtonActionHandler(() => !UsingKeyboard ? _device.LeftStickY > PlayerDeviceRecord.STICK_VERT_THRESHOLD : false, () => Input.GetAxis("Vertical") > PlayerDeviceRecord.STICK_VERT_THRESHOLD);
        _buttonActionHandlers[(int)ButtonActionTypes.MOVE_DIRECTION_DOWN] = new ButtonActionHandler(() => !UsingKeyboard ? _device.LeftStickY < -PlayerDeviceRecord.STICK_VERT_THRESHOLD : false, () => Input.GetAxis("Vertical") < -PlayerDeviceRecord.STICK_VERT_THRESHOLD);
    }

    public bool CompareDevice(InputDevice otherDevice)
    {
        return _device == otherDevice;
    }

    public bool GetButtonAction(ButtonActionTypes buttonActionType)
    {
        return _buttonActionHandlers[(int)buttonActionType].CheckButtonAction(UsingKeyboard);
    }

    public float GetLeftStickX()
    {
        return UsingKeyboard ? Input.GetAxis("Horizontal") : _device.LeftStickX;
    }

    public float GetLeftStickY()
    {
        return UsingKeyboard ? Input.GetAxis("Vertical") : _device.LeftStickY;
    }

    public class ButtonActionHandler
    {
        private ButtonActionCheck _deviceCheck;
        private ButtonActionCheck _keyboardCheck;

        public ButtonActionHandler(ButtonActionCheck deviceCheck, ButtonActionCheck keyboardCheck)
        {
            _deviceCheck = deviceCheck;
            _keyboardCheck = keyboardCheck;
        }

        public bool CheckButtonAction(bool usingKeyboard)
        {
            return usingKeyboard ? _keyboardCheck() : _deviceCheck();
        }
    }
}