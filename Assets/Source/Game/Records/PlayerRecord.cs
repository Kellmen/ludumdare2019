﻿using System;

public class PlayerRecord
{
    public Action PlayerUpdated;

    private PlayerDeviceRecord _deviceRecord;
    public PlayerDeviceRecord Device { get { return _deviceRecord; } set { _deviceRecord = value; if (PlayerUpdated != null) PlayerUpdated(); } }

    private int _lastLevelLoaded;
    public int LastLevelLoaded { get { return _lastLevelLoaded; } set { _lastLevelLoaded = value; if (PlayerUpdated != null) PlayerUpdated(); } }

    private bool _playerHasControlOfCharacter;
    public bool PlayerHasControlOfCharacter { get { return _playerHasControlOfCharacter; } set { _playerHasControlOfCharacter = value; if (PlayerUpdated != null) PlayerUpdated(); } }

    private int _currentLevel;
    public int CurrentLevel { get { return _currentLevel; } set { _currentLevel = value; if (PlayerUpdated != null) PlayerUpdated(); } }

    private bool _playerIsDead;
    public bool PlayerIsDead { get { return _playerIsDead; } set { _playerIsDead = value; if (PlayerUpdated != null) PlayerUpdated(); } }

    private string _debugPlayerLevel;
    public string DebugPlayerLevel { get { return _debugPlayerLevel; } set { _debugPlayerLevel = value; if (PlayerUpdated != null) PlayerUpdated(); } }

    public PlayerRecord()
    {
        _deviceRecord = new PlayerDeviceRecord();
        _deviceRecord.SetDevice(null);
        _lastLevelLoaded = 1;
        _playerHasControlOfCharacter = true;
        _currentLevel = 0;
        _debugPlayerLevel = "";
    }
}