﻿using System;
using UnityEngine;

[Serializable]
public class Resource
{
    public event Action Updated;
    [SerializeField]
    private int _amount;
    [SerializeField]
    private int _maximum;

    public delegate void EventHandler(bool hasIncreased, float change);
    public event EventHandler AmountChanged;

    public int Amount
    {
        get { return _amount; }
    }

    public event Action<int> MaximumChanged;
    public int Maximum
    {
        get { return _maximum; }
        set
        {
            if (value != _maximum)
            {
                _maximum = value;
                MaximumChanged?.Invoke(_maximum);
            }
        }
    }

    public Resource(int maximumResource)
    {
        _amount = maximumResource;
        _maximum = maximumResource;
    }

    public Resource(int currentResource, int maximumResource)
    {
        _amount = currentResource;
        _maximum = maximumResource;
    }

    public float Percent { get { return (float)Amount / Maximum; } }

    public void Add(int amount)
    {
        if (_amount >= _maximum) return;
        _amount = Mathf.Min(_amount + amount, _maximum);
        AmountChanged?.Invoke(true, amount);
        Updated?.Invoke();
    }

    /// <summary>
    /// Subtracts from the resource. If there is a remainder after fully depleted, it is returned for convenience.
    /// </summary>
    /// <param name="amount"></param>
    public int Subtract(int amount)
    {
        if (_amount == 0) return 0;
        int deduction = Mathf.Min(_amount, amount);
        int remainder = Mathf.Max(amount - deduction, 0);
        _amount -= deduction;
        AmountChanged?.Invoke(false, deduction);
        Updated?.Invoke();
        return remainder;
    }

    public void Fill()
    {
        if (_amount != _maximum)
        {
            float change = _maximum - _amount;
            _amount = _maximum;
            AmountChanged?.Invoke(true, change);
            Updated?.Invoke();
        }
    }

    public void Empty()
    {
        if (_amount != 0)
        {
            float change = _amount;
            _amount = 0;
            AmountChanged?.Invoke(false, change);
            Updated?.Invoke();
        }
    }
}