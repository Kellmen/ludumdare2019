﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [SerializeField]
    private List<TutorialMessage> _tutorialMessages;

    private IBaseGame _baseGame;

    void Awake()
    {
        _baseGame = BaseGame.Instance;
    }

    public void ShowTutorial(TutorialSteps step)
    {
        TurnOffTutorialText();
        _baseGame.DialogueService.RequestSkip();

        foreach (TutorialMessage message in _tutorialMessages)
        {
            if (message.TutorialStep == step)
            {
                if (message.TutorialSequence != null)
                {
                    BaseGame.Instance.DialogueService.QueueDialogueEvent(message.TutorialSequence);
                }

                if (message.TutorialText != null)
                {
                    message.TutorialText.SetActive(message.TutorialStep == step);
                }
            }
        }
    }

    public void TurnOffTutorialText()
    {
        foreach (TutorialMessage message in _tutorialMessages)
        {
            if (message.TutorialText != null)
            {
                message.TutorialText.SetActive(false);
            }
        }
    }
}

[Serializable]
public class TutorialMessage
{
    [SerializeField]
    private ConversationObject _tutorialSequence;

    [SerializeField]
    private GameObject _tutorialText;

    [SerializeField]
    private TutorialSteps _tutorialStep;

    public ConversationObject TutorialSequence { get { return _tutorialSequence; } } 
    public GameObject TutorialText { get { return _tutorialText; } }
    public TutorialSteps TutorialStep { get { return _tutorialStep; } }
}

public enum TutorialSteps
{
    NONE,
    MOVEMENT_CONTROLS,
    JUMP_CONTROLS,
    SELECT_EARTH_CARD,
    PLACE_BLOCK,
    SWITCH_ELEMENTS,
    FIRST_FIRE_ORB
}