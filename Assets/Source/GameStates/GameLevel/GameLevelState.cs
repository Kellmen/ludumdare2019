﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GameLevelState : IGameState
{
    private readonly IBaseGame _game;
    private readonly PlayingGameStateAssets _playingGameAssets;

    private GameController _levelController;
    private string _assetListName;

    private AsyncOperation _loadRequest;
    private Scene _gameLevelScene;

    private Action _onComplete;

    public GameLevelState(PlayingGameStateAssets playingGameAssets, Action onComplete)
    {
        _playingGameAssets = playingGameAssets;
        _onComplete = onComplete;

        _game = BaseGame.Instance;
    }

    private void OnSceneLoaded(AsyncOperation operation)
    {
        _loadRequest.completed -= OnSceneLoaded;

        _gameLevelScene = SceneManager.GetSceneByName("Level");
        SceneManager.SetActiveScene(_gameLevelScene);

        foreach (GameObject obj in _gameLevelScene.GetRootGameObjects())
        {
            _levelController = obj.GetComponent<GameController>();

            if (_levelController != null)
            {
                _levelController.Initialize(_playingGameAssets);
                break;
            }
        }
    }

    void IGameState.Start()
    {
        _loadRequest = SceneManager.LoadSceneAsync("Level", LoadSceneMode.Additive);
        _loadRequest.completed += OnSceneLoaded;
    }

    void IGameState.Stop()
    {
        _levelController = null;

        SceneManager.UnloadSceneAsync(_gameLevelScene);
        Resources.UnloadUnusedAssets();
    }

    void IGameState.Update()
    {

    }
}