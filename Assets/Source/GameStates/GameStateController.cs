﻿public sealed class GameStateController
{
    private IGameState _currentState;

    public void SwitchState(IGameState state)
    {
        if(_currentState != null)
            _currentState.Stop();

        _currentState = state;

        if (_currentState != null)
            _currentState.Start();
    }

    public void Update()
    {
        if (_currentState != null)
            _currentState.Update();
    }

    public void Stop()
    {
        if (_currentState != null)
            _currentState.Stop();

        _currentState = null;
    }
}