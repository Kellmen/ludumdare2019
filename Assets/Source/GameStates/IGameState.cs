﻿public interface IGameState
{
    void Start();
    void Update();
    void Stop();
}