﻿using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class MainMenuGameState : IGameState
{
    private readonly IBaseGame _game;

    private MainMenuView _view;
    private SinglePlayerSelectionController _singlePlayerSelectionController;

    private AsyncOperation _loadRequest;
    private Scene _mainMenuScene;

    public MainMenuGameState()
    {
        _game = BaseGame.Instance;
        _singlePlayerSelectionController = new SinglePlayerSelectionController(HandlePlayerDeviceSet);
    }

    private void OnSceneLoaded(AsyncOperation operation)
    {
        _loadRequest.completed -= OnSceneLoaded;
        _mainMenuScene = SceneManager.GetSceneByName("MainMenu");

        SceneManager.SetActiveScene(_mainMenuScene);

        foreach (GameObject obj in _mainMenuScene.GetRootGameObjects())
        {
            _view = obj.GetComponent<MainMenuView>();

            if (_view != null)
            {
                _view.PlayButton.onClick.AddListener(HandlePlayButton);
                break;
            }
        }
    }

    private void HandlePlayButton()
    {
        if (_loadRequest.isDone)
        {
            _game.SwitchState(new PlayingGameState());
        }
    }

    public void HandlePlayerDeviceSet(PlayerDeviceRecord playerDeviceRecord)
    {
        _game.RecordsService.Player.Device = playerDeviceRecord;
    }

    void IGameState.Start()
    {
        _loadRequest = SceneManager.LoadSceneAsync("MainMenu", LoadSceneMode.Additive);
        _loadRequest.completed += OnSceneLoaded;
    }

    void IGameState.Stop()
    {
        _view.PlayButton.onClick.RemoveListener(HandlePlayButton);
        _view = null;

        SceneManager.UnloadSceneAsync(_mainMenuScene);
        Resources.UnloadUnusedAssets();
    }

    void IGameState.Update()
    {
        _singlePlayerSelectionController.Update();

        if(_game.RecordsService.Player.Device.GetButtonAction(ButtonActionTypes.COMMAND_BUTTON_WAS_PRESSED))
        {
            HandlePlayButton();
        }
    }
}