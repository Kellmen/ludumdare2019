﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour
{
    [SerializeField]
    private Button _playButton;

    public Button PlayButton { get { return _playButton; } }
}
