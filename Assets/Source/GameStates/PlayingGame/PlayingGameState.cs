﻿using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class PlayingGameState : IGameState
{
    private readonly IBaseGame _game;

    private readonly GameStateController _stateController = new GameStateController();

    private PlayingGameStateAssets _playingGameAssets;

    private AsyncOperation _loadRequest;
    private Scene _gameScene;

    public PlayingGameState()
    {
        _game = BaseGame.Instance;
    }

    private void OnSceneLoaded(AsyncOperation operation)
    {
        _loadRequest.completed -= OnSceneLoaded;
        _gameScene = SceneManager.GetSceneByName("Game");

        SceneManager.SetActiveScene(_gameScene);

        _playingGameAssets = Resources.Load("PlayingGameStateAssets") as PlayingGameStateAssets;
        _stateController.SwitchState(new GameLevelState(_playingGameAssets, LoadNewLevel));
    }

    void LoadNewLevel()
    {
        _stateController.SwitchState(new GameLevelState(_playingGameAssets, LoadNewLevel));
    }

    void IGameState.Start()
    {
        _loadRequest = SceneManager.LoadSceneAsync("Game", LoadSceneMode.Additive);
        _loadRequest.completed += OnSceneLoaded;
    }

    void IGameState.Stop()
    {
        _stateController.Stop();

        SceneManager.UnloadSceneAsync(_gameScene);
        Resources.UnloadAsset(_playingGameAssets);
        Resources.UnloadUnusedAssets();
    }

    void IGameState.Update()
    {
        _stateController.Update();
    }
}