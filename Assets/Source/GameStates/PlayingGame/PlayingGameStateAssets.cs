﻿using UnityEngine;
using Lightbug.Kinematic2D.Implementation;

public sealed class PlayingGameStateAssets : ScriptableObject
{
    public const string KAddress = "PlayingGameAssets";

    [SerializeField]
    private CharacterController2D _playerCharacterPrefab;

    public CharacterController2D PlayerCharacterPrefab { get { return _playerCharacterPrefab; } }
}