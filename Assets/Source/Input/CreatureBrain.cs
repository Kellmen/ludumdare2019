﻿using Lightbug.Kinematic2D.Implementation;

public class CreatureBrain : CharacterBrain
{
    public bool Right
    {
        get { return characterAction.right; }
        set { characterAction.right = value; }
    }

    public bool Left
    {
        get { return characterAction.left; }
        set { characterAction.left = value; }
    }

    public bool Up
    {
        get { return characterAction.up; }
        set { characterAction.up = value; }
    }

    public bool Down
    {
        get { return characterAction.down; }
        set { characterAction.down = value; }
    }

    public bool JumpPressed
    {
        get { return characterAction.jumpPressed; }
        set { characterAction.jumpPressed = value; }
    }

    public bool JumpReleased
    {
        get { return characterAction.jumpReleased; }
        set { characterAction.jumpReleased = value; }
    }

    public override bool IsAI()
    {
        return false;
    }

    protected override void Update()
    {
    }
}