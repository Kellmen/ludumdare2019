﻿using Lightbug.Kinematic2D.Implementation;

public class InControlInputController : CharacterBrain
{
    private PlayerDeviceRecord _deviceRecord;
    private IBaseGame _game;

    public void Awake()
    {
        _game = BaseGame.Instance;
        _deviceRecord = _game.RecordsService.Player.Device;
    }

    public override bool IsAI()
    {
        return false;
    }

    protected override void Update()
    {
        if ((_deviceRecord == null && !_deviceRecord.UsingKeyboard) || !_game.RecordsService.Player.PlayerHasControlOfCharacter)
		{
			return;
		}

        characterAction.right |= _deviceRecord.GetButtonAction(ButtonActionTypes.MOVE_DIRECTION_RIGHT);
		characterAction.left |= _deviceRecord.GetButtonAction(ButtonActionTypes.MOVE_DIRECTION_LEFT);
		characterAction.up |= _deviceRecord.GetButtonAction(ButtonActionTypes.MOVE_DIRECTION_UP);
        characterAction.down |= _deviceRecord.GetButtonAction(ButtonActionTypes.MOVE_DIRECTION_DOWN);

        characterAction.jumpPressed |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_1_WAS_PRESSED);
        characterAction.jumpReleased |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_1_WAS_RELEASED);

        //characterAction.dashPressed |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_4_WAS_PRESSED);
        //characterAction.dashReleased |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_4_WAS_RELEASED);

        characterAction.shootWasPressed |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_2_WAS_PRESSED);
        characterAction.shootIsPressed |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_2_IS_PRESSED);
        characterAction.shootWasReleased |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_2_WAS_RELEASED);

        characterAction.vacuumWasPressed |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_3_WAS_PRESSED);
        characterAction.vacuumIsPressed |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_3_IS_PRESSED);
        characterAction.vacuumWasReleased |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_3_WAS_RELEASED);

        characterAction.Ability1WasPressed |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_4_WAS_PRESSED);
        characterAction.Ability1IsPressed |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_4_IS_PRESSED);
        characterAction.Ability1WasReleased |= _deviceRecord.GetButtonAction(ButtonActionTypes.ACTION_BUTTON_4_WAS_RELEASED);
    }
}