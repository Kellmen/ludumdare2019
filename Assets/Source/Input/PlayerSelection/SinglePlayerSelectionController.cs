﻿using InControl;
using System;
using UnityEngine;

public class SinglePlayerSelectionController
{
    private PlayerDeviceRecord _player;
    private Action<PlayerDeviceRecord> _playerDeviceSet;

    public SinglePlayerSelectionController(Action<PlayerDeviceRecord> playerDeviceSet)
    {
        _playerDeviceSet = playerDeviceSet;

        InputManager.OnDeviceDetached += OnDeviceDetached;
    }

    public void Update()
    {
        if (_player != null)
            return;

        var inputDevice = InputManager.ActiveDevice;

        if (KeyboardJoinButtonPressed())
        {
            _player = new PlayerDeviceRecord();
            _player.SetDevice(null);
            _playerDeviceSet(_player);
        }

        if (JoinButtonWasPressedOnDevice(inputDevice))
        {
            _player = new PlayerDeviceRecord();
            _player.SetDevice(inputDevice);
            _playerDeviceSet(_player);
        }
    }

    private bool KeyboardJoinButtonPressed()
    {
        return Input.GetKeyDown(KeyCode.Space);
    }

    private bool JoinButtonWasPressedOnDevice(InputDevice inputDevice)
    {
        return inputDevice.Action1.WasPressed || inputDevice.Action2.WasPressed || inputDevice.Action3.WasPressed ||
               inputDevice.Action4.WasPressed;
    }

    private void OnDeviceDetached(InputDevice inputDevice)
    {
        _player = null;
        _playerDeviceSet(_player);
    }
}