﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerializedLevelData
{
	public List<string> TileDataInJson = new List<string>();

	public void AddBlocks(List<SerializedTileData> blockDataList)
	{
		for (int i = 0; i < blockDataList.Count; i++)
		{
			AddBlock(blockDataList[i]);
		}
	}

	public void AddBlock(SerializedTileData blockData)
	{
		TileDataInJson.Add(JsonUtility.ToJson(blockData));
	}
}
