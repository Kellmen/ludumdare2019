﻿using UnityEngine;

public class SerializedTileData
{
	public Vector2Int GridPosition;
	public string AssetName;
}