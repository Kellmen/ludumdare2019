﻿using UnityEngine;
using UnityEditor;

public interface ITileAsset
{
    string Name { get; set; }
    Sprite BlockSprite { get; set; }
}