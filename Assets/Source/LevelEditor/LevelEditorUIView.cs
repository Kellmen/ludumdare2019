﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelEditorUIView : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup _canvasGroup;

    [SerializeField]
    private GridManager _gridManager;

    [Header("Loading")]
    [SerializeField]
    private Button _menuLoadButton;

    [SerializeField]
    private Button _loadLevelButton;

    [SerializeField]
    private TMP_Dropdown _loadLevelDropdown;

    [SerializeField]
    private GameObject _loadLevelPanel;

    [SerializeField]
    private Button _loadLevelCloseButton;

    [Header("Saving")]
    [SerializeField]
    private Button _menuSaveButton;

    [SerializeField]
    private Button _menuSaveAsButton;

    [SerializeField]
    private Button _saveAsButton;

    [SerializeField]
    private TMP_InputField _saveAsNameInput;

    [SerializeField]
    private GameObject _saveAsPanel;

    [SerializeField]
    private Button _saveAsCloseButton;

    [Header("Create New")]
    [SerializeField]
    private Button _menuCreateLevelButton;

    [SerializeField]
    private Button _createLevelButton;

    [SerializeField]
    private TMP_InputField _createLevelNameInput;

    [SerializeField]
    private GameObject _createNewLevelPanel;

    [SerializeField]
    private Button _createNewCloseButton;

    [Header("Info")]
    [SerializeField]
    private Button _editorInfoButton;

    [SerializeField]
    private GameObject _editorInfoPanel;

    [SerializeField]
    private Button _editorInfoCloseButton;

    [Header("Other Menu Items")]
    [SerializeField]
    private Button _eraserButton;

    [SerializeField]
    private TMP_Dropdown _tileMapLayerDropdown;

    [SerializeField]
    private Button _layerVisiblityButton;

    [SerializeField]
    private Image _eyeImage;

    [SerializeField]
    private GameObject _unsavedIcon;

    [Header("Panels")]
    [SerializeField]
    private Transform _tilePrefabListContentParent;

    [SerializeField]
    private Transform _tileListContentParent;

    [SerializeField]
    private TileListElementController _blockListElementObject;

    [SerializeField]
    private List<RectTransform> _nonClickableRegions;

    [SerializeField]
    private Animator _prefabPanelAnimator;

    [SerializeField]
    private Animator _tilePanelAnimator;

    [SerializeField]
    private Button _prefabPanelExpandButton;

    [SerializeField]
    private Button _tilePanelExpandButton;

    [Header("Extra UI")]
    [SerializeField]
    private TMP_Text _currentLevelText;

    [SerializeField]
    private GameObject _inputBlocker;

    private IBaseGame _baseGame;

    private List<TileListElementController> _blockListElements = new List<TileListElementController>();

    private List<TilePrefabAssetObject> _tilePrefabAssetObjects = new List<TilePrefabAssetObject>();
    private List<TileAssetObject> _tileAssetObjects = new List<TileAssetObject>();

    private Vector2 _localMousePosition;
    private ITileAsset _lastTileType;
    private bool _prefabPanelShowing;
    private bool _tilePanelShowing;

    public bool Erasing { get; private set; }

    public Button MenuLoadButton { get { return _menuLoadButton; } }
    public Button LoadLevelButton { get { return _loadLevelButton; } }
    public TMP_Dropdown LoadLevelDropdown { get { return _loadLevelDropdown; } }
    public GameObject LoadLevelPanel { get { return _loadLevelPanel; } }
    public Button LoadLevelCloseButton { get { return _loadLevelCloseButton; } }
    public TMP_Dropdown TileMapLayerDropdown { get { return _tileMapLayerDropdown; } }

    public Button MenuSaveButton { get { return _menuSaveButton; } }

    public Button MenuCreateLevelButton { get { return _menuCreateLevelButton; } }
    public Button CreateLevelButton { get { return _createLevelButton; } }
    public TMP_InputField CreateLevelNameInput { get { return _createLevelNameInput; } }
    public GameObject CreateNewLevelPanel { get { return _createNewLevelPanel; } }
    public Button CreateNewCloseButton { get { return _createNewCloseButton; } }

    public Button MenuSaveAsButton { get { return _menuSaveAsButton; } }
    public Button SaveAsButton { get { return _saveAsButton; } }
    public TMP_InputField SaveAsNameInput { get { return _saveAsNameInput; } }
    public GameObject SaveAsPanel { get { return _saveAsPanel; } }
    public Button SaveAsCloseButton { get { return _saveAsCloseButton; } }

    public Button EditorInfoButton { get { return _editorInfoButton; } }
    public Button EditorInfoCloseButton { get { return _editorInfoCloseButton; } }
    public GameObject EditorInfoPanel { get { return _editorInfoPanel; } }

    public Button LayerVisiblityButton { get { return _layerVisiblityButton; } }
    public Image EyeImage { get { return _eyeImage; } }

    public TMP_Text CurrentLevelText { get { return _currentLevelText; } }
    public GameObject InputBlocker { get { return _inputBlocker; } }
    public GameObject UnsavedIcon { get { return _unsavedIcon; } }

    void Awake()
    {
        _baseGame = BaseGame.Instance;
        _unsavedIcon.SetActive(false);
    }

    void Start()
    {
        foreach (KeyValuePair<string, ITileAsset> obj in _gridManager.PrefabTileAssetObjects)
        {
            TileListElementController listElement = null;
            if (obj.Value is TilePrefabAssetObject)
            {
                listElement = Instantiate(_blockListElementObject, _tilePrefabListContentParent, false);
            }
            else if (obj.Value is TileAssetObject)
            {
                listElement = Instantiate(_blockListElementObject, _tileListContentParent, false);
            }

            listElement.Initialize(obj.Value);
            listElement.BlockClicked += HandleBlockButtonPressed;
            _blockListElements.Add(listElement);
        }

        _tileMapLayerDropdown.AddOptions(_gridManager.TileMapLayerNames);
        _tileMapLayerDropdown.value = _gridManager.CurrentTileMap;

        _prefabPanelExpandButton.onClick.AddListener(HandlePrefabPanelButtonClicked);
        _tilePanelExpandButton.onClick.AddListener(HandleTilePanelButtonClicked);
        _eraserButton.onClick.AddListener(HandleEraserButtonClicked);

        _prefabPanelAnimator.Play("Hidden");
        _tilePanelAnimator.Play("Hidden");
        _prefabPanelShowing = false;
        _tilePanelShowing = false;
    }

    void Update()
    {
        bool hoveringNoClickRegion = false;
        foreach (RectTransform rectTrans in _nonClickableRegions)
        {
            _localMousePosition = rectTrans.InverseTransformPoint(Input.mousePosition);
            if (rectTrans.rect.Contains(_localMousePosition))
            {
                _gridManager.MouseFollowingElement.TurnOff();
                hoveringNoClickRegion = true;
            }
        }

        if (!hoveringNoClickRegion)
        {
            if (Erasing)
            {
                _gridManager.MouseFollowingElement.ShowEraser();
            }
            else if (_lastTileType != _gridManager.MouseFollowingElement.CurrentTileType)
            {
                _gridManager.MouseFollowingElement.SetTileType(_lastTileType);
            }
        }
    }

    void OnDestroy()
    {
        foreach (TileListElementController element in _blockListElements)
        {
            element.BlockClicked -= HandleBlockButtonPressed;
        }

        _prefabPanelExpandButton.onClick.RemoveListener(HandlePrefabPanelButtonClicked);
        _tilePanelExpandButton.onClick.RemoveListener(HandleTilePanelButtonClicked);
        _eraserButton.onClick.RemoveListener(HandleEraserButtonClicked);
    }

    public void DoReset()
    {
        _prefabPanelShowing = false;
        _tilePanelShowing = false;
    }

    public void SetInteractable(bool interactable)
    {
        _canvasGroup.interactable = interactable;
    }

    public void CancelCurrentSelection()
    {
        _gridManager.MouseFollowingElement.TurnOff();
        _lastTileType = null;
        Erasing = false;
    }

    private void HandleBlockButtonPressed(TileListElementController elementController)
    {
        ITileAsset tileAsset = null;
        if (_gridManager.PrefabTileAssetObjects.TryGetValue(elementController.BlockAsset.Name, out tileAsset))
        {
            if (tileAsset is TilePrefabAssetObject)
            {
                _gridManager.SetActiveTileMap("Level");
            }
        }

        _lastTileType = elementController.BlockAsset;
        Erasing = false;
    }

    private void HandlePrefabPanelButtonClicked()
    {
        _prefabPanelAnimator.Play(_prefabPanelShowing ? "Hide" : "Show");
        _prefabPanelShowing = !_prefabPanelShowing;
    }

    private void HandleTilePanelButtonClicked()
    {
        _tilePanelAnimator.Play(_tilePanelShowing ? "Hide" : "Show");
        _tilePanelShowing = !_tilePanelShowing;
    }

    private void HandleEraserButtonClicked()
    {
        _gridManager.MouseFollowingElement.ShowEraser();
        _lastTileType = null;
        Erasing = true;
    }
}