﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class TileAssetObject : ScriptableObject, ITileAsset
{
    public Tile BlockTile;

    private Sprite _blockSprite;
    public Sprite BlockSprite { get { return _blockSprite; } set { _blockSprite = value; } }

    public string Name { get; set; }

    public void Awake()
    {
        Name = this.name;
        _blockSprite = BlockTile.sprite;
    }
}