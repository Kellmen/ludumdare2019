﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TileListElementController : MonoBehaviour
{
    [SerializeField]
    private Image _blockImage;

    [SerializeField]
    private Button _blockButton;

    public ITileAsset BlockAsset { get; private set; }

    public event Action<TileListElementController> BlockClicked;

    public void Initialize(ITileAsset blockAsset)
    {
        BlockAsset = blockAsset;
        _blockImage.sprite = BlockAsset.BlockSprite;

        _blockButton.onClick.AddListener(HandleBlockButtonClicked);
    }

    private void OnDestroy()
    {
        _blockButton.onClick.RemoveListener(HandleBlockButtonClicked);
    }

    private void HandleBlockButtonClicked()
    {
        BlockClicked?.Invoke(this);
    }
}