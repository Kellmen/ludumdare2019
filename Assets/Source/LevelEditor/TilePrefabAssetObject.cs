﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class TilePrefabAssetObject : ScriptableObject, ITileAsset
{
    [SerializeField]
    private GameObject _mouseFollowObject;

    [SerializeField]
    private GameObject _blockPrefab;

    [SerializeField]
    private Tile _blockTile;

    [SerializeField]
    private Sprite _blockSprite;

    public GameObject MouseFollowObject { get { return _mouseFollowObject; } }
    public GameObject BlockPrefab { get { return _blockPrefab; } }
    public Tile BlockTile { get { return _blockTile; } }
    public Sprite BlockSprite { get { return _blockSprite; } set { _blockSprite = value; } }

    public string Name { get; set; }

    public void Awake()
    {
        Name = this.name;
    }
}