﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public enum InGameStates
{
    INTRO,
    PLAYING,
    EDITOR,
    COMPLETE
}

public class LevelInfo
{
    public string Name { get; private set; }
    public string Text { get; set; }

    public LevelInfo(string name, string text)
    {
        Name = name;
        Text = text;
    }
}

public class GameController : MonoBehaviour
{
    [SerializeField]
    private GameView _view;

    [SerializeField]
    private List<TextAsset> _levelJsonFiles;

    private IBaseGame _baseGame;

    private GameStateController _stateController = new GameStateController();
    private int _currentLevel = -1;

    public int GameLevelCount { get; private set; }
    public List<LevelInfo> LevelInfos { get; private set; }

    public void Initialize(PlayingGameStateAssets playingGameAssets)
    {
        _baseGame = BaseGame.Instance;

        LevelInfos = new List<LevelInfo>();
        _view.PlayingGameAssets = playingGameAssets;
        _view.GameLevelAudio.InitMusicTracks();

        foreach (TextAsset textAsset in _levelJsonFiles)
        {
            LevelInfos.Add(new LevelInfo(textAsset.name, textAsset.text));
        }

        string[] filePaths = Directory.GetFiles(Application.persistentDataPath + @"\LevelFiles\", "*.txt", SearchOption.TopDirectoryOnly);

        foreach (string filePath in filePaths)
        {
            LevelInfos.Add(new LevelInfo(filePath.Substring(filePath.LastIndexOf('\\') + 1).Replace(".txt", ""), GetJsonTextForLevel(filePath)));
        }

        // Only count the levels in Resources folder in overall level count
        GameLevelCount = _levelJsonFiles.Count + filePaths.Length;

        // Intro sequence state will either change or be deleted
        //SwitchState(_baseGame.RecordsService.Player.CurrentLevel == 0 ? InGameStates.INTRO : InGameStates.PLAYING);
        SwitchState(InGameStates.PLAYING);
    }

    void OnDestroy()
    {
        StopAllCoroutines();
        _stateController.Stop();
    }

    void Update()
    {
        _stateController.Update();
    }

    public void SwitchState(InGameStates state)
    {
        switch (state)
        {
            //case InGameStates.INTRO:
            //    _stateController.SwitchState(new InGameOpeningSequenceState(_view, this));
            //    break;
            case InGameStates.PLAYING:
                _stateController.SwitchState(new InGamePlayingState(_view, this));
                break;
            case InGameStates.EDITOR:
                _stateController.SwitchState(new InGameEditorState(_view, this));
                break;
            case InGameStates.COMPLETE:
                _stateController.SwitchState(new InGameCompleteState(_view, this));
                break;
        }
    }

    public void RevealLevel(bool initializing)
    {
        _view.GridManager.ClearAllTiles();
        _view.TutorialManager.TurnOffTutorialText();

        if (!_baseGame.DialogueService.IsDialoguePlaying())
        {
            _view.GameLevelAudio.PlayGoalSound();
        }

        _currentLevel = _baseGame.RecordsService.Player.CurrentLevel;

        if (initializing && !String.IsNullOrEmpty(_baseGame.RecordsService.Player.DebugPlayerLevel))
        {
            _view.GridManager.DeActivateTileMap(() => LoadLevel(_baseGame.RecordsService.Player.DebugPlayerLevel, () => _view.GridManager.ActivateTileMap(TileMapRevealComplete)), initializing ? 1 : 0.02f);
        }
        else
        {
            _view.GridManager.DeActivateTileMap(() => LoadLevel(_currentLevel, () => _view.GridManager.ActivateTileMap(TileMapRevealComplete)), initializing ? 1 : 0.02f);
        }
    }

    public void LoadLevel(string levelName, Action onComplete)
    {
        int index = LevelInfos.FindIndex(info => info.Name == levelName);

        if (index >= 0)
        {
            LoadLevel(index, onComplete);
        }
        else
        {
            Debug.LogError("LEVEL NAME " + levelName + " DOES NOT EXIST!");
            onComplete?.Invoke();
            return;
        }
    }

    public void LoadLevel(int levelIndex, Action onComplete)
    {
        if (LevelInfos[levelIndex] == null)
        {
            Debug.LogError("LEVEL INDEX " + levelIndex + " DOES NOT EXIST!");
            return;
        }

        string levelText = LevelInfos[levelIndex].Text;

        if (!string.IsNullOrEmpty(levelText))
        {
            SerializedLevelData levelData = JsonUtility.FromJson<SerializedLevelData>(levelText);

            for (int i = 0; i < levelData.TileDataInJson.Count; i++)
            {
                SerializedTileData blockData = JsonUtility.FromJson<SerializedTileData>(levelData.TileDataInJson[i]);
				Vector3 cellPosition = _view.GridManager.GameGrid.CellToWorld((Vector3Int)blockData.GridPosition);
				cellPosition = new Vector3(cellPosition.x + _view.GridManager.GameGrid.cellSize.x / 2f, cellPosition.y + _view.GridManager.GameGrid.cellSize.y / 2f, 0f);

				if (blockData.AssetName == "PlayerSpawnAsset")
				{
					cellPosition = new Vector3(cellPosition.x, cellPosition.y, _view.PlayerCharacter.CharacterBody.bodyTransform.Position.z);
					_view.PlayerCharacter.Teleport(cellPosition, Quaternion.identity);
				}
				else if (blockData.AssetName == "GoalSpawnAsset")
				{
					cellPosition = new Vector3(cellPosition.x, cellPosition.y, _view.GoalOrb.transform.position.z);
					_view.GoalOrb.transform.position = cellPosition;
				}

				foreach (KeyValuePair<string, ITileAsset> blockAssetObject in _view.GridManager.PrefabTileAssetObjects)
				{
					if (blockAssetObject.Value.Name == blockData.AssetName)
					{
						_view.GridManager.PlaceTile(blockAssetObject.Value, (Vector3Int)blockData.GridPosition);
						break;
					}
				}
            }

            _baseGame.RecordsService.Player.CurrentLevel = levelIndex;

            onComplete?.Invoke();
        }
        else
        {
            Debug.LogError("FILE FOR LEVEL " + levelIndex + " DOES NOT EXTST!");
        }

        _view.LevelEditorUI.SetInteractable(true);
    }

    public void SaveLevel(string levelName, string jsonData, Action onComplete)
    {
        string filePath = Application.persistentDataPath + "/LevelFiles";

        if (!Directory.Exists(filePath))
        {
            Directory.CreateDirectory(filePath);
        }

        string levelText = GetCurrentLevelJson();

        File.WriteAllText(filePath + "/" + levelName + ".txt", jsonData);

        int index = LevelInfos.FindIndex(info => info.Name == levelName);

        if (index >= 0)
        {
            LevelInfos[index].Text = levelText;
        }
        else
        {
            LevelInfos.Add(new LevelInfo(levelName, jsonData));
        }

        onComplete?.Invoke();
    }

    public void CreateNewLevel(string levelName, Action onComplete)
    {
        CreateNewLevel(levelName, JsonUtility.ToJson(GetDefaultLevelData()), onComplete);
    }

    public void CreateNewLevel(string levelName, string jsonData, Action onComplete)
    {
        _view.LevelEditorUI.SetInteractable(false);

        SaveLevel(levelName, jsonData, onComplete);
        GameLevelCount++;
        LoadLevel(levelName, onComplete);
    }

    public string GetCurrentLevelJson()
    {
        SerializedLevelData levelData = new SerializedLevelData();

        // Never save empty level files, since we use that to determine loading
        if (_view.GridManager.GridTiles.Count > 0)
        {
            foreach (GridTile tile in _view.GridManager.GridTiles)
            {
                SerializedTileData newBlockData = new SerializedTileData();
                newBlockData.GridPosition = (Vector2Int)tile.Coordinate;
                newBlockData.AssetName = tile.AssetObjectName;
                levelData.AddBlock(newBlockData);
            }
        }
        else
        {
            levelData = GetDefaultLevelData();
        }

        return JsonUtility.ToJson(levelData);
    }

    public SerializedLevelData GetDefaultLevelData()
    {
        SerializedLevelData levelData = new SerializedLevelData();
        SerializedTileData newBlockData = new SerializedTileData();

        newBlockData.GridPosition = new Vector2Int(-1, -5);
        newBlockData.AssetName = "EarthBlockAsset";
        levelData.AddBlock(newBlockData);

        return levelData;
    }

    private string GetJsonTextForLevel(string path)
    {
        string levelText = "";

        if (File.Exists(path))
        {
            levelText = File.ReadAllText(path);
        }

        return levelText;
    }

    private void TileMapRevealComplete()
    {
        _view.GoalOrb.SetActive(true);
    }
}