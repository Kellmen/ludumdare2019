﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Com.LuisPedroFonseca.ProCamera2D;
using Lightbug.Kinematic2D.Implementation;

public class GameView : MonoBehaviour
{
    [SerializeField]
    private Button _stuckButton;

    [SerializeField]
    private Button _exitGameButton;

    [SerializeField]
    private SpawnAnchor _playerSpawnAnchor;

    [SerializeField]
    private Transform _playerSpawnLocation;

    [SerializeField]
    private Transform _characterOffScreenSpawnPoint;

    [SerializeField]
    private TutorialManager _tutorialManager;

    [SerializeField]
    private GameLevelAudio _gameLevelAudio;

    [SerializeField]
    private List<ConversationObject> _introSequences;

    [SerializeField]
    private ProCamera2D _gameCamera;

    [SerializeField]
    private ProCamera2D _levelEditorCamera;

    [SerializeField]
    private ProCamera2DPanAndZoom _levelEditorPanAndZoom;

    [SerializeField]
    private Animator _gameCompleteAnimator;

    [SerializeField]
    private GridManager _gridManager;

    [SerializeField]
    private LevelEditorUIView _levelEditorUI;

    [SerializeField]
    private GameObject _goalOrb;

    public Button StuckButton { get { return _stuckButton; } }
    public Button ExitGameButton { get { return _exitGameButton; } }
    public SpawnAnchor PlayerSpawnAnchor { get { return _playerSpawnAnchor; } }
    public Transform PlayerSpawnLocation { get { return _playerSpawnLocation; } }
    public Transform CharacterOffScreenSpawnPoint { get { return _characterOffScreenSpawnPoint; } }
    public TutorialManager TutorialManager { get { return _tutorialManager; } }
    public GameLevelAudio GameLevelAudio { get { return _gameLevelAudio; } }
    public List<ConversationObject> IntroSequences { get { return _introSequences; } }
    public ProCamera2D GameCamera { get { return _gameCamera; } }
    public ProCamera2D LevelEditorCamera { get { return _levelEditorCamera; } }
    public ProCamera2DPanAndZoom LevelEditorPanAndZoom { get { return _levelEditorPanAndZoom; } }
    public Animator GameCompleteAnimator { get { return _gameCompleteAnimator; } }
    public GridManager GridManager { get { return _gridManager; } }
    public LevelEditorUIView LevelEditorUI { get { return _levelEditorUI; } }
    public GameObject GoalOrb { get { return _goalOrb; } }

    public PlayingGameStateAssets PlayingGameAssets { get; set; }

    public CharacterController2D PlayerCharacter
    {
        get
        {
            return _playerCharacter;
        }

        set
        {
            _playerCharacter = value;
            _baseGame.CommonGameElementsService.RegisterCommonElement("player", _playerCharacter.GetComponent<PlayerEntity>());
        }
    }
    private CharacterController2D _playerCharacter;

    private IBaseGame _baseGame;

    void Awake()
    {
        _baseGame = BaseGame.Instance;
    }
}