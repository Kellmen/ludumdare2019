﻿public class InGameCompleteState : IGameState
{
    private IBaseGame _baseGame;
    private GameController _controller;
    private GameView _view;

    public InGameCompleteState(GameView view, GameController controller)
    {
        _view = view;
        _controller = controller;
        _baseGame = BaseGame.Instance;
    }

    void IGameState.Start()
    {
        _view.ExitGameButton.onClick.AddListener(CloseGame);

        _baseGame.RecordsService.Player.PlayerHasControlOfCharacter = false;
        _baseGame.RecordsService.Player.CurrentLevel = 0;

        _view.GameLevelAudio.PlayGameComplete();
        _view.GameCompleteAnimator.Play("FadeIn");
    }

    void IGameState.Stop()
    {
    }

    void IGameState.Update()
    {
    }

    private void CloseGame()
    {
        _view.ExitGameButton.onClick.RemoveListener(CloseGame);
        _baseGame.AudioService.StopAll();
        _baseGame.SwitchState(new MainMenuGameState());
    }
}