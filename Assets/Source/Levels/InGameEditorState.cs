﻿using System.Collections.Generic;
using UnityEngine;

public class InGameEditorState : IGameState
{
    private IBaseGame _baseGame;
    private GameController _controller;
    private GameView _view;

    private float _stateChangeDelay;

    private Vector3 _mouseWorldPos;
    private Vector3Int _mousePosCell;

    public InGameEditorState(GameView view, GameController controller)
    {
        _view = view;
        _controller = controller;
        _baseGame = BaseGame.Instance;
    }

    void IGameState.Start()
    {
        _baseGame.RecordsService.Player.PlayerHasControlOfCharacter = false;

        _view.GameCamera.gameObject.SetActive(false);
        _view.LevelEditorCamera.gameObject.SetActive(true);
        _view.LevelEditorPanAndZoom.PanTarget.gameObject.transform.position = _view.GameCamera.gameObject.transform.position;

        _view.PlayerCharacter.OnTriggerEnterHit += HandleOnPlayerTriggerEnterHit;
        _view.GridManager.OnTileMapChanged += HandleTileMapChanged;

        _view.LevelEditorUI.MenuSaveButton.onClick.AddListener(HandleMenuSaveButton);
        _view.LevelEditorUI.MenuLoadButton.onClick.AddListener(HandleMenuLoadButton);
        _view.LevelEditorUI.LoadLevelButton.onClick.AddListener(HandleLoadLevelButton);
        _view.LevelEditorUI.LoadLevelCloseButton.onClick.AddListener(HandlePanelCloseButton);

        _view.LevelEditorUI.MenuCreateLevelButton.onClick.AddListener(HandleMenuCreateLevelButton);
        _view.LevelEditorUI.CreateLevelButton.onClick.AddListener(HandleCreateLevelButton);
        _view.LevelEditorUI.CreateNewCloseButton.onClick.AddListener(HandlePanelCloseButton);

        _view.LevelEditorUI.MenuSaveAsButton.onClick.AddListener(HandleMenuSaveAsButton);
        _view.LevelEditorUI.SaveAsButton.onClick.AddListener(HandleSaveAsButton);
        _view.LevelEditorUI.SaveAsCloseButton.onClick.AddListener(HandlePanelCloseButton);

        _view.LevelEditorUI.EditorInfoButton.onClick.AddListener(HandleEditorInfoButton);
        _view.LevelEditorUI.EditorInfoCloseButton.onClick.AddListener(HandlePanelCloseButton);

        _view.LevelEditorUI.TileMapLayerDropdown.onValueChanged.AddListener(HandleTileMapLayerDropDownChanged);

        _view.LevelEditorUI.LayerVisiblityButton.onClick.AddListener(HandleLayerVisiblityButton);

        SetupLevelDropdownList();

        _stateChangeDelay = Time.time + 1.0f;

        _view.LevelEditorUI.gameObject.SetActive(true);
        _view.LevelEditorUI.DoReset();
    }

    void IGameState.Stop()
    {
        _controller.LevelInfos[_baseGame.RecordsService.Player.CurrentLevel].Text = _controller.GetCurrentLevelJson();

        _view.PlayerCharacter.OnTriggerEnterHit -= HandleOnPlayerTriggerEnterHit;
        _view.GridManager.OnTileMapChanged -= HandleTileMapChanged;

        _view.LevelEditorUI.MenuSaveButton.onClick.RemoveListener(HandleMenuSaveButton);
        _view.LevelEditorUI.MenuLoadButton.onClick.RemoveListener(HandleMenuLoadButton);
        _view.LevelEditorUI.LoadLevelButton.onClick.RemoveListener(HandleLoadLevelButton);
        _view.LevelEditorUI.LoadLevelCloseButton.onClick.RemoveListener(HandlePanelCloseButton);

        _view.LevelEditorUI.MenuCreateLevelButton.onClick.RemoveListener(HandleMenuCreateLevelButton);
        _view.LevelEditorUI.CreateLevelButton.onClick.RemoveListener(HandleCreateLevelButton);
        _view.LevelEditorUI.CreateNewCloseButton.onClick.RemoveListener(HandlePanelCloseButton);

        _view.LevelEditorUI.MenuSaveAsButton.onClick.RemoveListener(HandleMenuSaveAsButton);
        _view.LevelEditorUI.SaveAsButton.onClick.RemoveListener(HandleSaveAsButton);
        _view.LevelEditorUI.SaveAsCloseButton.onClick.RemoveListener(HandlePanelCloseButton);

        _view.LevelEditorUI.EditorInfoButton.onClick.RemoveListener(HandleEditorInfoButton);
        _view.LevelEditorUI.EditorInfoCloseButton.onClick.RemoveListener(HandlePanelCloseButton);

        _view.LevelEditorUI.TileMapLayerDropdown.onValueChanged.RemoveListener(HandleTileMapLayerDropDownChanged);

        _view.LevelEditorUI.LayerVisiblityButton.onClick.RemoveListener(HandleLayerVisiblityButton);

        _view.GridManager.MouseFollowingElement.TurnOff();
    }

    void IGameState.Update()
    {
        _view.LevelEditorUI.CurrentLevelText.text = "Level: " + _controller.LevelInfos[_baseGame.RecordsService.Player.CurrentLevel].Name;

        if (_view.GameCamera.gameObject.activeInHierarchy)
        {
            _mouseWorldPos = _view.GameCamera.GameCamera.ScreenToWorldPoint(Input.mousePosition);
        }
        else if (_view.LevelEditorCamera.gameObject.activeInHierarchy)
        {
            _mouseWorldPos = _view.LevelEditorCamera.GameCamera.ScreenToWorldPoint(Input.mousePosition);
        }

        // Eventually change this to use player input class
        if (Input.GetKey(KeyCode.Mouse0) && (_view.GridManager.MouseFollowingElement.CurrentTileType != null || _view.LevelEditorUI.Erasing))
        {
            _mousePosCell = _view.GridManager.GameGrid.WorldToCell(_mouseWorldPos);

            // Are we trying to place a tile of the same type over top another
            if (_view.LevelEditorUI.Erasing || _view.GridManager.MouseFollowingElement.CurrentTileType != null)
            {
                _view.GridManager.RemoveTile(_mousePosCell);

                if (!_view.LevelEditorUI.Erasing && _view.GridManager.GetCurrentTileMapTile(_mousePosCell) == null && _view.GridManager.MouseFollowingElement.CurrentTileType != null)
                {
                    if (_view.GridManager.PrefabTileAssetObjects.TryGetValue(_view.GridManager.MouseFollowingElement.CurrentTileType.Name, out ITileAsset obj))
					{
						Vector3 cellPosition = _view.GridManager.GameGrid.CellToWorld(_mousePosCell);
						cellPosition = new Vector3(cellPosition.x + _view.GridManager.GameGrid.cellSize.x / 2f, cellPosition.y + _view.GridManager.GameGrid.cellSize.y / 2f, 0f);

						if (_view.GridManager.MouseFollowingElement.CurrentTileType.Name == "PlayerSpawnAsset")
						{
							cellPosition = new Vector3(cellPosition.x, cellPosition.y, _view.PlayerCharacter.CharacterBody.bodyTransform.Position.z);
							_view.PlayerCharacter.Teleport(cellPosition, Quaternion.identity);
						}
						else if (_view.GridManager.MouseFollowingElement.CurrentTileType.Name == "GoalSpawnAsset")
						{
							cellPosition = new Vector3(cellPosition.x, cellPosition.y, _view.GoalOrb.transform.position.z);
							_view.GoalOrb.transform.position = cellPosition;
						}

						_view.GridManager.PlaceTile(obj, _mousePosCell);
                    }
                }

                _view.LevelEditorUI.UnsavedIcon.SetActive(true);
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1) && Time.time > _stateChangeDelay)
        {
            _controller.SwitchState(InGameStates.PLAYING);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _view.PlayerCharacter.Teleport(new Vector3(_mouseWorldPos.x, _mouseWorldPos.y, _view.PlayerCharacter.CharacterBody.bodyTransform.Position.z), Quaternion.identity);

        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            _view.LevelEditorUI.CancelCurrentSelection();
        }
    }

    private void HandleOnPlayerTriggerEnterHit(Collider2D collider)
    {
        if (collider.CompareTag("Death"))
        {
            _view.PlayerCharacter.OnTriggerEnterHit -= HandleOnPlayerTriggerEnterHit;

            if (_view.PlayerCharacter != null && _view.PlayerCharacter.CharacterAnimation != null && _view.PlayerCharacter.CharacterAnimation.gameObject != null)
            {
                GameObject.Destroy(_view.PlayerCharacter.CharacterAnimation.gameObject);
            }
            if (_view.PlayerCharacter != null && _view.PlayerCharacter.gameObject != null)
            {
                GameObject.Destroy(_view.PlayerCharacter.gameObject);
            }
        }
    }

    private void HandleMenuSaveButton()
    {
        _controller.SaveLevel(_controller.LevelInfos[_baseGame.RecordsService.Player.CurrentLevel].Name, _controller.GetCurrentLevelJson(), HandleSaveLevelComplete);
    }

    private void HandleMenuLoadButton()
    {
        ShowPanel("Load");
    }

    private void HandleLoadLevelButton()
    {
        _view.LevelEditorUI.SetInteractable(false);

        _view.GridManager.ClearAllTiles();
        _controller.LoadLevel(_controller.LevelInfos[_view.LevelEditorUI.LoadLevelDropdown.value].Name, HandleLoadLevelComplete);
    }

    private void HandleMenuCreateLevelButton()
    {
        ShowPanel("CreateNew");
    }

    private void HandleCreateLevelButton()
    {
        if (!string.IsNullOrEmpty(_view.LevelEditorUI.CreateLevelNameInput.text))
        {
            _view.GridManager.ClearAllTiles();
            _controller.CreateNewLevel(_view.LevelEditorUI.CreateLevelNameInput.text, HandleCreateLevelComplete);
        }
    }

    private void HandleMenuSaveAsButton()
    {
        ShowPanel("SaveAs");
    }

    private void HandleSaveAsButton()
    {
        _controller.CreateNewLevel(_view.LevelEditorUI.SaveAsNameInput.text, _controller.GetCurrentLevelJson(), HandleCreateLevelComplete);
    }

    private void HandleEditorInfoButton()
    {
        ShowPanel("Info");
    }

    private void HandlePanelCloseButton()
    {
        TurnOffPanels();
    }

    private void HandleLayerVisiblityButton()
    {
        if (_view.LevelEditorUI.EyeImage.color.a < 1)
        {
            _view.LevelEditorUI.EyeImage.color = new Color(_view.LevelEditorUI.EyeImage.color.r, _view.LevelEditorUI.EyeImage.color.g, _view.LevelEditorUI.EyeImage.color.b, 1);

            foreach (string layer in _view.GridManager.TileMapLayerNames)
            {
                _view.GridManager.SetTileMapAlpha(layer, 1);
            }
        }
        else
        {
            _view.LevelEditorUI.EyeImage.color = new Color(_view.LevelEditorUI.EyeImage.color.r, _view.LevelEditorUI.EyeImage.color.g, _view.LevelEditorUI.EyeImage.color.b, 0.25f);

            foreach (string layer in _view.GridManager.TileMapLayerNames)
            {
                _view.GridManager.SetTileMapAlpha(layer, layer == _view.GridManager.TileMapLayerNames[_view.GridManager.CurrentTileMap] ? 1 : 0);
            }
        }
    }

    private void ShowPanel(string panel)
    {
        _view.LevelEditorUI.CancelCurrentSelection();

        switch (panel)
        {
            case "SaveAs":
                _view.LevelEditorUI.LoadLevelPanel.SetActive(false);
                _view.LevelEditorUI.CreateNewLevelPanel.SetActive(false);
                _view.LevelEditorUI.SaveAsPanel.SetActive(true);
                _view.LevelEditorUI.EditorInfoPanel.SetActive(false);
                break;

            case "CreateNew":
                _view.LevelEditorUI.LoadLevelPanel.SetActive(false);
                _view.LevelEditorUI.CreateNewLevelPanel.SetActive(true);
                _view.LevelEditorUI.SaveAsPanel.SetActive(false);
                _view.LevelEditorUI.EditorInfoPanel.SetActive(false);
                break;

            case "Load":
                _view.LevelEditorUI.LoadLevelPanel.SetActive(true);
                _view.LevelEditorUI.CreateNewLevelPanel.SetActive(false);
                _view.LevelEditorUI.SaveAsPanel.SetActive(false);
                _view.LevelEditorUI.EditorInfoPanel.SetActive(false);
                break;

            case "Info":
                _view.LevelEditorUI.LoadLevelPanel.SetActive(false);
                _view.LevelEditorUI.CreateNewLevelPanel.SetActive(false);
                _view.LevelEditorUI.SaveAsPanel.SetActive(false);
                _view.LevelEditorUI.EditorInfoPanel.SetActive(true);
                break;
        }

        _view.LevelEditorUI.InputBlocker.SetActive(true);
    }

    private void TurnOffPanels()
    {
        _view.LevelEditorUI.LoadLevelPanel.SetActive(false);
        _view.LevelEditorUI.CreateNewLevelPanel.SetActive(false);
        _view.LevelEditorUI.SaveAsPanel.SetActive(false);
        _view.LevelEditorUI.EditorInfoPanel.SetActive(false);

        _view.LevelEditorUI.InputBlocker.SetActive(false);
    }

    private void HandleLoadLevelComplete()
    {
        _view.LevelEditorUI.UnsavedIcon.SetActive(false);
        TurnOffPanels();
    }

    private void HandleCreateLevelComplete()
    {
        _view.LevelEditorUI.UnsavedIcon.SetActive(false);
        SetupLevelDropdownList();
        TurnOffPanels();
    }

    private void HandleSaveLevelComplete()
    {
        _view.LevelEditorUI.UnsavedIcon.SetActive(false);
        SetupLevelDropdownList();
        TurnOffPanels();
    }

    private void HandleTileMapLayerDropDownChanged(int index)
    {
        _view.GridManager.SetActiveTileMap(_view.GridManager.TileMapLayerNames[index]);
    }

    private void HandleTileMapChanged()
    {
        _view.LevelEditorUI.TileMapLayerDropdown.value = _view.GridManager.CurrentTileMap;
    }

    private void SetupLevelDropdownList()
    {
        _view.LevelEditorUI.LoadLevelDropdown.ClearOptions();

        List<string> levelOptions = new List<string>();

        foreach (LevelInfo info in _controller.LevelInfos)
        {
            levelOptions.Add(info.Name);
        }

        _view.LevelEditorUI.LoadLevelDropdown.AddOptions(levelOptions);
    }

    private string GetCurrentLevelJson()
    {
        SerializedLevelData levelData = new SerializedLevelData();

        // Never save empty level files, since we use that to determine loading
        if (_view.GridManager.GridTiles.Count > 0)
        {
            foreach (GridTile tile in _view.GridManager.GridTiles)
            {
                SerializedTileData newBlockData = new SerializedTileData();
                newBlockData.GridPosition = (Vector2Int)tile.Coordinate;
                newBlockData.AssetName = tile.AssetObjectName;
                levelData.AddBlock(newBlockData);
            }
        }
        else
        {
            levelData = _controller.GetDefaultLevelData();
        }

        return JsonUtility.ToJson(levelData);
    }
}