﻿using UnityEngine;
using Lightbug.Kinematic2D.Implementation;

public class InGameOpeningSequenceState : IGameState
{
    private IBaseGame _baseGame;
    private GameController _controller;
    private GameView _view;

    private int _completedSequencesCount = 0;

    public InGameOpeningSequenceState(GameView view, GameController controller)
    {
        _view = view;
        _controller = controller;
        _baseGame = BaseGame.Instance;
    }

    void IGameState.Start()
    {
        _baseGame.RecordsService.Player.PlayerHasControlOfCharacter = false;

        _controller.RevealLevel(false);

        _view.IntroSequences[_completedSequencesCount].OnComplete += HandleSequenceComplete;
        BaseGame.Instance.DialogueService.QueueDialogueEvent(_view.IntroSequences[_completedSequencesCount]);
    }

    void IGameState.Stop()
    {
        if (_view.PlayerCharacter != null)
        {
            _view.PlayerCharacter.OnTriggerEnterHit -= HandleOnPlayerTriggerEnterHit;
        }

        for(int i = 0; i < _view.IntroSequences.Count; ++i)
        {
            _view.IntroSequences[i].OnComplete -= HandleSequenceComplete;
        }
    }

    void IGameState.Update()
    {
    }

    private void HandleSequenceComplete()
    {
        _completedSequencesCount++;

        if (_view.PlayerCharacter == null)
        {
            _view.PlayerCharacter = GameObject.Instantiate<CharacterController2D>(_view.PlayingGameAssets.PlayerCharacterPrefab, _view.CharacterOffScreenSpawnPoint);
            _view.PlayerCharacter.Teleport(new Vector3(_view.PlayerSpawnLocation.transform.position.x, _view.PlayerSpawnLocation.transform.position.y, _view.PlayerSpawnLocation.transform.position.z), Quaternion.identity);
            _view.PlayerSpawnAnchor.SetTarget(_view.PlayerCharacter.gameObject.transform);
            _view.PlayerCharacter.OnTriggerEnterHit += HandleOnPlayerTriggerEnterHit;
        }

        if (_completedSequencesCount < _view.IntroSequences.Count)
        {
            _view.IntroSequences[_completedSequencesCount].OnComplete += HandleSequenceComplete;
            BaseGame.Instance.DialogueService.QueueDialogueEvent(_view.IntroSequences[_completedSequencesCount]);
        }
        else if (_completedSequencesCount == _view.IntroSequences.Count)
        {
            _view.TutorialManager.ShowTutorial(TutorialSteps.MOVEMENT_CONTROLS);
            _controller.SwitchState(InGameStates.PLAYING);
        }
    }

    private void HandleCharacterDeath(bool skipDeathSounds)
    {
        _view.GameLevelAudio.PlayDeathSound(false);

        _view.PlayerSpawnAnchor.SetTarget(null);
        _view.PlayerCharacter.OnTriggerEnterHit -= HandleOnPlayerTriggerEnterHit;

        if (_view.PlayerCharacter != null && _view.PlayerCharacter.CharacterAnimation != null && _view.PlayerCharacter.CharacterAnimation.gameObject != null)
            GameObject.Destroy(_view.PlayerCharacter.CharacterAnimation.gameObject);
        if (_view.PlayerCharacter != null && _view.PlayerCharacter.gameObject != null)
            GameObject.Destroy(_view.PlayerCharacter.gameObject);
    }

    private void HandleOnPlayerTriggerEnterHit(Collider2D collider)
    {
        if (collider.CompareTag("Death"))
        {
            HandleCharacterDeath(false);
        }
    }
}