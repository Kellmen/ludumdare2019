﻿using Lightbug.Kinematic2D.Implementation;
using System.Collections;
using UnityEngine;

public class InGamePlayingState : IGameState
{
    private IBaseGame _baseGame;

    private GameController _controller;
    private GameView _view;

    private IEnumerator _doRespawnPlayer;

    private Vector3 _mouseWorldPos;
    private Vector3Int _mousePosCell;

    private float _lastDeathVOPlayTime;
    private float _delayBeforeAnotherDeathVO = 5.0f;
    private bool _initializing;
    private float _stateChangeDelay;

    public InGamePlayingState(GameView view, GameController controller)
    {
        _baseGame = BaseGame.Instance;

        _view = view;
        _controller = controller;
    }

    void IGameState.Start()
    {
        _stateChangeDelay = Time.time + 0f;

        _baseGame.RecordsService.Player.PlayerHasControlOfCharacter = true;

        _view.GameCamera.gameObject.SetActive(true);
        _view.LevelEditorCamera.gameObject.SetActive(false);

        _view.LevelEditorUI.gameObject.SetActive(false);
        _view.StuckButton.onClick.AddListener(HandleStuckButton);

		InitPlayer();
		_controller.RevealLevel(true);

        _view.PlayerCharacter.OnTriggerEnterHit += HandleOnPlayerTriggerEnterHit;

        if (_baseGame.RecordsService.Player.CurrentLevel > 3)
        {
            _view.StuckButton.gameObject.SetActive(true);
        }
    }

    void IGameState.Stop()
    {
        _view.GridManager.MouseFollowingElement.TurnOff();

        _view.StuckButton.gameObject.SetActive(false);
        _view.StuckButton.onClick.RemoveListener(HandleStuckButton);

        if (_view.PlayerCharacter != null)
        {
            _view.PlayerCharacter.OnTriggerEnterHit -= HandleOnPlayerTriggerEnterHit;
        }

        _doRespawnPlayer = null;
    }

    void IGameState.Update()
    {
        if (_doRespawnPlayer != null && !_doRespawnPlayer.MoveNext())
        {
            _doRespawnPlayer = null;
        }

        if (Input.GetKeyDown(KeyCode.Alpha1) && Time.time > _stateChangeDelay)
        {
            _controller.SwitchState(InGameStates.EDITOR);
        }
    }

    private void InitPlayer()
    {
        if (_view.PlayerCharacter == null)
        {
            _view.PlayerCharacter = GameObject.Instantiate<CharacterController2D>(_view.PlayingGameAssets.PlayerCharacterPrefab, _view.CharacterOffScreenSpawnPoint);
            _view.PlayerCharacter.Teleport(new Vector3(_view.PlayerSpawnLocation.transform.position.x, _view.PlayerSpawnLocation.transform.position.y, _view.PlayerSpawnLocation.transform.position.z), Quaternion.identity);
            _view.PlayerSpawnAnchor.SetTarget(_view.PlayerCharacter.gameObject.transform);
        }
    }

    private void HandleOnPlayerTriggerEnterHit(Collider2D collider)
    {
        if (collider.CompareTag("Death"))
        {
            HandleCharacterDeath(false);
        }

        if (collider.CompareTag("Goal"))
        {
            _view.GoalOrb.SetActive(false);
            HandleCharacterDeath(true);

            _view.GameLevelAudio.PlayGoalCollectSound();
            _baseGame.RecordsService.Player.CurrentLevel += 1;

            if (_baseGame.RecordsService.Player.CurrentLevel > _controller.GameLevelCount)
            {
                _controller.SwitchState(InGameStates.COMPLETE);
            }
            else
            {
                if (_baseGame.RecordsService.Player.CurrentLevel > 3)
                {
                    _view.StuckButton.gameObject.SetActive(true);
                }

                // Don't let the death voice over overlap
                _lastDeathVOPlayTime = Time.time;
                _controller.RevealLevel(false);
            }
        }
    }

    private void HandleCharacterDeath(bool skipDeathSounds)
    {
        _view.StuckButton.gameObject.SetActive(false);

        if (!skipDeathSounds && _baseGame.RecordsService.Player.CurrentLevel > 3 && !_baseGame.DialogueService.IsDialoguePlaying())
        {
            if (Time.time > _lastDeathVOPlayTime + _delayBeforeAnotherDeathVO)
            {
                _view.GameLevelAudio.PlayDeathSound(true);
                _lastDeathVOPlayTime = Time.time;
            }
            else
            {
                _view.GameLevelAudio.PlayDeathSound(false);
            }
        }
        else
        {
            _view.GameLevelAudio.PlayDeathSound(false);
        }

        _view.PlayerSpawnAnchor.SetTarget(null);
        _view.PlayerCharacter.OnTriggerEnterHit -= HandleOnPlayerTriggerEnterHit;

        if (_view.PlayerCharacter != null && _view.PlayerCharacter.CharacterAnimation != null && _view.PlayerCharacter.CharacterAnimation.gameObject != null)
            GameObject.Destroy(_view.PlayerCharacter.CharacterAnimation.gameObject);
        if (_view.PlayerCharacter != null && _view.PlayerCharacter.gameObject != null)
            GameObject.Destroy(_view.PlayerCharacter.gameObject);

        _doRespawnPlayer = DoRespawnPlayer();

        _view.GridManager.ResetDestroyableTiles();
    }

    private IEnumerator DoRespawnPlayer()
    {
        float time = 0f;
        while (time < 1.0f)
        {
            time += Time.deltaTime;
            yield return null;
        }

        _view.PlayerCharacter = GameObject.Instantiate<CharacterController2D>(_view.PlayingGameAssets.PlayerCharacterPrefab, _view.CharacterOffScreenSpawnPoint);
        _view.PlayerCharacter.OnTriggerEnterHit += HandleOnPlayerTriggerEnterHit;
        _view.PlayerCharacter.Teleport(new Vector3(_view.PlayerSpawnLocation.transform.position.x, _view.PlayerSpawnLocation.transform.position.y, _view.PlayerSpawnLocation.transform.position.z), Quaternion.identity);
        _view.PlayerSpawnAnchor.SetTarget(_view.PlayerCharacter.gameObject.transform);

        if (_baseGame.RecordsService.Player.CurrentLevel > 2)
        {
            _view.StuckButton.gameObject.SetActive(true);
        }
    }

    private void HandleStuckButton()
    {
        HandleCharacterDeath(true);
    }
}