﻿using UnityEngine;

public class NpcController : MonoBehaviour, IDialogueSpeaker
{
    [SerializeField]
    private ConversationObject _conversationInfo;

    ConversationObject IDialogueSpeaker.GetConversation()
    {
        return _conversationInfo;
    }
}