﻿using UnityEngine;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour
{
    [SerializeField]
    private Transform _poolParentObject;

    private List<PoolableObject> _objectList;

    void Awake()
    {
        _objectList = new List<PoolableObject>();
    }

    /// <summary>
    /// Sets the pooled objects parent to the new parent and returns a reference
    /// </summary>
    public PoolableObject GetObjectFromPool(PoolableObject objectToFind)//, Transform newParent)
    {
        PoolableObject pObj = null;

        for (int i = 0; i < _objectList.Count; ++i)
        {
            if (_objectList[i].AssetListName == objectToFind.AssetListName && _objectList[i].PooledObjectIdentifier == objectToFind.PooledObjectIdentifier)
            {
                pObj = _objectList[i];
                //_objectList[i].transform.SetParent(newParent, false);
                _objectList[i].gameObject.SetActive(true);
                _objectList.RemoveAt(i);
                return pObj;
            }
        }

		return Instantiate<PoolableObject>(objectToFind, _poolParentObject, false);//, newParent, false);
	}

	public PoolableObject GetObjectFromPool(PooledObjectIdentifiers pooledObjectIdentifier)//, Transform newParent)
	{
		PoolableObject pObj = null;

		for (int i = 0; i < _objectList.Count; ++i)
		{
			if (_objectList[i].PooledObjectIdentifier == pooledObjectIdentifier)
			{
				pObj = _objectList[i];
				//_objectList[i].transform.SetParent(newParent, false);
				_objectList[i].gameObject.SetActive(true);
				_objectList.RemoveAt(i);
				return pObj;
			}
		}

		return null;
	}

	/// <summary>
	/// Add existing object to pool
	/// </summary>
	public void AddObjectToPool(PoolableObject poolableObject)
    {
        poolableObject.transform.SetParent(_poolParentObject, false);
        poolableObject.gameObject.SetActive(false);

        _objectList.Add(poolableObject);
    }

    /// <summary>
    /// Destroy all objects in pool.
    /// </summary>
    public void ClearPool()
    {
        for (int i = 0; i < _objectList.Count; i++)
        {
            Destroy(_objectList[i].gameObject);
        }
        _objectList.Clear();
    }
}

public enum PooledObjectIdentifiers
{
	BaseProjectile
}