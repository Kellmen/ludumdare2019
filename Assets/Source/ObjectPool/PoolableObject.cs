﻿using UnityEngine;

public class PoolableObject : MonoBehaviour
{
    [SerializeField]
    private string _assetListName;

    [SerializeField]
    private PooledObjectIdentifiers _pooledObjectIdentifier;

    public string AssetListName { get { return _assetListName; } }
    public PooledObjectIdentifiers PooledObjectIdentifier { get { return _pooledObjectIdentifier; } }
}