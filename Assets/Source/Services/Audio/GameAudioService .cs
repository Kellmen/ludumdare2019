﻿using System.Collections.Generic;
using UnityEngine;

public class GameAudioService : IGameAudioService
{
    public List<SoundInfo> GameSounds = new List<SoundInfo>();

    private IBaseGame _game;
    private GameAudioServiceView _gameAudioServiceView;

    public GameAudioService(IBaseGame game, GameAudioServiceView gameAudioServiceView)
    {
        _game = game;
        _gameAudioServiceView = gameAudioServiceView;
    }

    void IGameAudioService.Add(SoundInfo sound)
    {
        sound.source = _gameAudioServiceView.AudioRoot.gameObject.AddComponent<AudioSource>();
        sound.source.clip = sound.clip;

        sound.source.volume = sound.Volume;
        sound.source.pitch = sound.Pitch;
        sound.source.loop = sound.Loop;

        GameSounds.Add(sound);
    }

    void IGameAudioService.Remove(SoundInfo sound)
    {
        GameSounds.Remove(sound);
    }

    void IGameAudioService.RemoveByName(string clipName)
    {
        SoundInfo toRemove = GameSounds.Find((SoundInfo snd) => snd.name == clipName);
        if (toRemove != null)
        {
            GameSounds.Remove(toRemove);
        }
    }

    void IGameAudioService.StopAll()
    {
        foreach (SoundInfo sound in GameSounds)
        {
            if (sound.source.isPlaying)
            {
                sound.source.volume = 0;
                sound.source.Stop();
            }
        }
    }

    void IGameAudioService.Stop(string clipName)
    {
        SoundInfo sound = GameSounds.Find(gameSound => gameSound.name == clipName);

        if (sound == null)
            Debug.LogWarning("Sound: " + clipName + " not found!");
        else
            sound.source.Stop();
    }

    void IGameAudioService.Play(string clipName)
    {
        SoundInfo sound = GameSounds.Find(gameSound => gameSound.name == clipName);

        if (sound == null)
            Debug.LogWarning("Sound: " + clipName + " not found!");
        else
            sound.source.Play();
    }

    bool IGameAudioService.IsPlaying(string clipName)
    {
        SoundInfo sound = GameSounds.Find(gameSound => gameSound.name == clipName);

        if (sound == null)
        {
            Debug.LogWarning("Sound: " + clipName + " not found!");
            return false;
        }
        else
        {
            return sound.source.isPlaying;
        }
    }

    bool IGameAudioService.IsAnySoundPlaying()
    {
        bool soundplaying = false;

        foreach (SoundInfo sound in GameSounds)
        {
            if (sound.source.isPlaying)
            {
                soundplaying = true;
            }
        }

        return soundplaying;
    }

    void IGameAudioService.SetTrackVolume(string clipName, float volume)
    {
        SoundInfo sound = GameSounds.Find(gameSound => gameSound.name == clipName);

        if (sound == null)
            Debug.LogWarning("Sound: " + clipName + " not found!");
        else
            sound.source.volume = volume;
    }

    void IGameAudioService.TurnOffByKey(string key)
    {
        foreach (SoundInfo sound in GameSounds)
        {
            if (sound.name.Contains(key))
            {
                sound.source.Stop();
            }
        }
    }
}

[System.Serializable]
public class SoundInfo
{
    public string name;
    public AudioClip clip;

    [Range(0f, 1f)]
    public float Volume = 1f;

    [Range(0.1f, 3f)]
    public float Pitch = 1f;

    public bool Loop;

    [HideInInspector]
    public AudioSource source;
}