﻿using UnityEngine;
using System.Collections;

public class GameAudioServiceView : MonoBehaviour
{
    [SerializeField]
    private Transform _audioRoot;

    public Transform AudioRoot { get { return _audioRoot; } }
}
