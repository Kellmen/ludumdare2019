﻿public interface IGameAudioService 
{
    void Add(SoundInfo sound);
    void Remove(SoundInfo sound);
    void RemoveByName(string clipName);
    void SetTrackVolume(string clipName, float volume);
    void TurnOffByKey(string key);
    void Play(string clipName);
    void Stop(string clipName);
    void StopAll();
    bool IsPlaying(string clipName);
    bool IsAnySoundPlaying();
}