﻿using System.Collections.Generic;
using UnityEngine;

public class DialogueService : IDialogueService
{
    private readonly IBaseGame _game;
    private readonly DialogueServiceView _view;

    private List<ConversationObject> QueuedConversations = new List<ConversationObject>();
    private List<DialogueMessage> dialogueMessages = new List<DialogueMessage>();
    private DialogueMessage _currentDialogueMessage;
    private ConversationObject _currentConversation;

    private bool _waitingForResponseChoice;

    private float _playerReponseWaitTime;
    private float _playerReponseTimeInterval = 0.25f;

    private int _selectedResponseIndex = 0;

    public DialogueService(IBaseGame game, DialogueServiceView view)
    {
        _game = game;
        _view = view;

        _view.ResponeButtonsOnClick += HandlePlayerDialogueChoice;
    }

    void HandlePlayerDialogueChoice(DialogueMessage followUp)
    {
        _waitingForResponseChoice = false;
        dialogueMessages.Insert(0, followUp);
        CheckDisplayNextMessage();
    }

    public void CheckDisplayNextMessage()
    {
        if (!_waitingForResponseChoice && !_view.DisplayingMessage && dialogueMessages.Count > 0)
        {
            _currentDialogueMessage = dialogueMessages[0];
            dialogueMessages.RemoveAt(0);

            _view.DisplayNextMessage(_currentDialogueMessage, MessageComplete);
        }
        else if (!_waitingForResponseChoice && !_view.DisplayingMessage && dialogueMessages.Count == 0)
        {
            _currentConversation.OnComplete?.Invoke();
            _currentConversation = null;
            _view.ClearMessages();

            if (QueuedConversations.Count > 0)
            {
                StartNextConversation();
            }
            else
            {
                QueuedConversations.Clear();
            }
        }
    }

    public void MessageComplete()
    {
        _playerReponseWaitTime = Time.time + _playerReponseTimeInterval;

        if (_view.GetActiveResponseButtonCount() > 0)
        {
            _waitingForResponseChoice = true;
            _selectedResponseIndex = 0;

            for (int i = 0; i < _view.PlayerResponseButtons.Count; ++i)
            {
                _view.PlayerResponseButtons[i].Selected = (i == _selectedResponseIndex);
            }
        }
        else if (_currentDialogueMessage.AutoContinue)
        {
            _waitingForResponseChoice = false;
            CheckDisplayNextMessage();
        }
    }

    private void StartNextConversation()
    {
        _currentConversation = QueuedConversations[0];
        QueuedConversations.RemoveAt(0);

        foreach (DialogueMessage message in _currentConversation.DialogueMessages)
        {
            dialogueMessages.Add(message);
        }

        CheckDisplayNextMessage();
    }

    void IDialogueService.RequestSkip()
    {
        _view.RequestSkip();
    }

    bool IDialogueService.IsDialoguePlaying()
    {
        return _view.DisplayingMessage || _currentConversation != null;
    }

    void IDialogueService.QueueDialogueEvent(ConversationObject conversation)
    {
        if (QueuedConversations.Contains(conversation)) return;
        QueuedConversations.Add(conversation);

        if (_currentConversation == null)
        {
            StartNextConversation();
        }
    }
}