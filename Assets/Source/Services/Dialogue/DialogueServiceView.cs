﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueServiceView : MonoBehaviour
{
    private const float kAutoContinueDefaultWaitTime = 1.25f;

    [SerializeField]
    private TMPro.TextMeshProUGUI _messageField;

    [SerializeField]
    private List<PlayerResponseButton> _playerResponseButtons;

    [SerializeField]
    private CanvasGroup _messageGroup;

    private IBaseGame _baseGame;
    private bool _skip;

    public List<PlayerResponseButton> PlayerResponseButtons { get { return _playerResponseButtons; } }
    public bool DisplayingMessage { get; private set; }

    public Action<DialogueMessage> ResponeButtonsOnClick;

    private IEnumerator something;

    public void Start()
    {
        _baseGame = BaseGame.Instance;
    }

    public void DisplayNextMessage(DialogueMessage dialogueMessage, Action onComplete)
    {
        StartCoroutine(DoDisplayNextMessage(dialogueMessage, onComplete));

        something = DoDisplayNextMessage(dialogueMessage, onComplete);
    }

    private IEnumerator DoDisplayNextMessage(DialogueMessage dialogueMessage, Action onComplete)
    {
        DisplayingMessage = true;
        _skip = false;

        if (dialogueMessage.VoiceOver.clip != null)
        {
            _baseGame.AudioService.Add(dialogueMessage.VoiceOver);
        }

        foreach (PlayerResponseButton btn in _playerResponseButtons)
        {
            btn.ResponseButton.gameObject.SetActive(false);
        }

        _messageField.maxVisibleCharacters = 0;
        _messageField.text = dialogueMessage.Message;

        // Wait one frame for the text mesh text to update
        yield return null;

        int totalVisibleCharacters = _messageField.textInfo.characterCount;
        _messageGroup.alpha = 1;

        yield return new WaitForSeconds(dialogueMessage.StartDelay);

        if (dialogueMessage.VoiceOver.clip != null)
        {
            _baseGame.AudioService.Play(dialogueMessage.VoiceOver.name);
        }

        int visibleCount = 0;
        while (visibleCount < totalVisibleCharacters)
        {
            if (_skip)
            {
                visibleCount = totalVisibleCharacters;
                _messageField.maxVisibleCharacters = visibleCount;
                break;
            }
            else
            {
                ++visibleCount;
            }

            _messageField.maxVisibleCharacters = visibleCount;

            yield return new WaitForSeconds(0.05f);
        }

        if (!_skip)
        {
            yield return new WaitForSeconds(dialogueMessage.AutoContinue ? dialogueMessage.EndDelay > 0 ? dialogueMessage.EndDelay : kAutoContinueDefaultWaitTime : 0);
        }

        if (dialogueMessage.PlayerResponseChoices.Count > 0)
        {
            for (int i = 0; i < dialogueMessage.PlayerResponseChoices.Count; ++i)
            {
                _playerResponseButtons[i].ChoiceText.text = dialogueMessage.PlayerResponseChoices[i].Response;
                _playerResponseButtons[i].FollowUp = dialogueMessage.PlayerResponseChoices[i].FollowUpMessage;
                _playerResponseButtons[i].ResponseButton.gameObject.SetActive(true);
            }
        }

        if (!_skip)
        {
            while (_baseGame.AudioService.IsPlaying(dialogueMessage.VoiceOver.name))
            {
                yield return null;
            }
        }

        _baseGame.AudioService.Stop(dialogueMessage.VoiceOver.name);
        _baseGame.AudioService.RemoveByName(dialogueMessage.VoiceOver.name);

        DisplayingMessage = false;
        onComplete?.Invoke();
    }

    public void RequestSkip()
    {
        _skip = true;
    }

    // Called from inspector
    public void OnChoiceButtonClicked(int buttonId)
    {
        ResponeButtonsOnClick?.Invoke(_playerResponseButtons.Find((PlayerResponseButton btn) => btn.ButtonIndex == buttonId).FollowUp);
    }

    public int GetActiveResponseButtonCount()
    {
        int count = 0;
        foreach (PlayerResponseButton btn in _playerResponseButtons)
        {
            count += btn.ResponseButton.gameObject.activeInHierarchy ? 1 : 0;
        }

        return count;
    }

    public void ClearMessages()
    {
        StopAllCoroutines();

        _messageField.text = "";
        _messageGroup.alpha = 0;

        for (int i = 0; i < _playerResponseButtons.Count; ++i)
        {
            _playerResponseButtons[i].ResponseButton.gameObject.SetActive(false);
            _playerResponseButtons[i].ChoiceText.text = "";
            _playerResponseButtons[i].FollowUp = null;
        }

        DisplayingMessage = false;
    }
}