﻿public interface IDialogueService
{
    void QueueDialogueEvent(ConversationObject conversation);
    void RequestSkip();
    bool IsDialoguePlaying();
}