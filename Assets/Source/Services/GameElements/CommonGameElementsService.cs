﻿using System.Collections.Generic;

public class CommonGameElementsService : ICommonGameElementsService
{
    public Dictionary<string, object> GameElements { get; private set; } = new Dictionary<string, object>();

    public void RegisterCommonElement<T>(string key, T value)
    {
        GameElements[key] = value;
    }

    public bool TryGetCommonElement<T>(string key, out T value)
    {
        if (GameElements.TryGetValue(key, out object val) && val is T)
        {
            value = (T)val;
            return true;
        }

        value = default;
        return false;
    }
}