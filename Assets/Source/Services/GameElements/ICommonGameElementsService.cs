﻿public interface ICommonGameElementsService
{
    void RegisterCommonElement<T>(string key, T value);
    bool TryGetCommonElement<T>(string key, out T value);
}