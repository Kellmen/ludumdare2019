﻿using UnityEngine;
using System.Collections;

public interface IRecordsService 
{
    void InitializeRecords();
    PlayerRecord Player { get; }
}
