﻿using UnityEngine;
using System.Collections;

public class RecordsService : IRecordsService
{
    private IBaseGame _game;

    public PlayerRecord Player { get; private set; }

    public RecordsService(IBaseGame game)
    {
        _game = game;
    }

    void IRecordsService.InitializeRecords()
    {
        Player = new PlayerRecord();
    }
}