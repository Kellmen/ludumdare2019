﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActAsWallWhenPushedAgainstAWallStrategy : Strategy
{
	private readonly BaseEntity _entity;
	private readonly Vector2 _rayCastOffset;

	private RaycastHit2D _rightObject;
	private RaycastHit2D _leftObject;

	private Vector3 _entityGroundNormal;

	private LayerMask _layerMaskCreature = LayerMask.GetMask("Creature");
	private LayerMask _layerMaskCreatureObstacle = LayerMask.GetMask("Creature Obstacle");
	private LayerMask _layerMaskStaticObstacle = LayerMask.GetMask("Static Obstacle");
	float _objectCheckDistance = 0.08f;
	float _directionSwitchCooldown = 0.25f;
	float _directionSwitchWaitTime = 0f;
	float _tolerance = 0.001f;

	public ActAsWallWhenPushedAgainstAWallStrategy(CreatureEntity entity, Vector2 rayCastOffset)
	{
		_entity = entity;
		_rayCastOffset = rayCastOffset;
	}

	protected override void OnStart() { }

	protected override void OnStop() { }

	private bool Approximately(float floatOne, float floatTwo)
	{
		return Mathf.Abs(floatOne - floatTwo) <= _tolerance;
	}

	public override void Update()
	{
		if (Time.time > _directionSwitchWaitTime)
		{
			bool shouldTurnAround = ShouldTurnAroundFromCollision(_entity, _layerMaskCreature, new List<int>() { _entity.GetInstanceID() });

			if (!shouldTurnAround)
			{
				shouldTurnAround = ShouldTurnAroundFromCollision(_entity, _layerMaskCreatureObstacle, new List<int>() { _entity.GetInstanceID() });
			}

			if (!shouldTurnAround)
			{
				shouldTurnAround = ShouldTurnAroundFromCollision(_entity, _layerMaskStaticObstacle, new List<int>() { _entity.GetInstanceID() });
			}

			if (shouldTurnAround)
			{
				Vector2 newFacingDirection = Vector2.right;

				if (_entity.FacingDirection == Vector2.right)
				{
					newFacingDirection = Vector2.left;
				}

				_entity.SetFacingDirection(newFacingDirection, false);
				_directionSwitchWaitTime = Time.time + _directionSwitchCooldown;
			}
		}
	}

	public bool ShouldTurnAroundFromCollision(BaseEntity entity, LayerMask layerMask, List<int> alreadyCheckedGameObjectIDs)
	{
		bool recursiveCollision = false;

		if (entity.CharacterController.IsGrounded)
		{
			_entityGroundNormal = entity.CharacterController.GroundNormal;

			// up
			if (Approximately(_entityGroundNormal.x, 0) && Approximately(_entityGroundNormal.y, 1))
			{
				_rightObject = Physics2D.Raycast(new Vector2(entity.transform.position.x + _rayCastOffset.x, entity.transform.position.y + _rayCastOffset.y), Vector2.right, _objectCheckDistance, layerMask);
				_leftObject = Physics2D.Raycast(new Vector2(entity.transform.position.x - _rayCastOffset.x, entity.transform.position.y + _rayCastOffset.y), Vector2.left, _objectCheckDistance, layerMask);

				Debug.DrawRay(new Vector2(entity.transform.position.x + _rayCastOffset.x, entity.transform.position.y + _rayCastOffset.y), Vector2.right, Color.magenta);
				Debug.DrawRay(new Vector2(entity.transform.position.x - _rayCastOffset.x, entity.transform.position.y + _rayCastOffset.y), Vector2.left, Color.magenta);
			}
			// down
			if (Approximately(_entityGroundNormal.x, 0) && Approximately(_entityGroundNormal.y, -1))
			{
				_rightObject = Physics2D.Raycast(new Vector2(entity.transform.position.x - _rayCastOffset.x, entity.transform.position.y - _rayCastOffset.y), Vector2.left, _objectCheckDistance, layerMask);
				_leftObject = Physics2D.Raycast(new Vector2(entity.transform.position.x + _rayCastOffset.x, entity.transform.position.y - _rayCastOffset.y), Vector2.right, _objectCheckDistance, layerMask);

				Debug.DrawRay(new Vector2(entity.transform.position.x - _rayCastOffset.x, entity.transform.position.y - _rayCastOffset.y), Vector2.left, Color.blue);
				Debug.DrawRay(new Vector2(entity.transform.position.x + _rayCastOffset.x, entity.transform.position.y - _rayCastOffset.y), Vector2.right, Color.blue);
			}
			// right
			if (Approximately(_entityGroundNormal.x, 1) && Approximately(_entityGroundNormal.y, 0))
			{
				_rightObject = Physics2D.Raycast(new Vector2(entity.transform.position.x + _rayCastOffset.y, entity.transform.position.y - _rayCastOffset.x), Vector2.down, _objectCheckDistance, layerMask);
				_leftObject = Physics2D.Raycast(new Vector2(entity.transform.position.x + _rayCastOffset.y, entity.transform.position.y + _rayCastOffset.x), Vector2.up, _objectCheckDistance, layerMask);

				Debug.DrawRay(new Vector2(entity.transform.position.x + _rayCastOffset.y, entity.transform.position.y - _rayCastOffset.x), Vector2.down, Color.green);
				Debug.DrawRay(new Vector2(entity.transform.position.x + _rayCastOffset.y, entity.transform.position.y + _rayCastOffset.x), Vector2.up, Color.green);
			}
			// left
			if (Approximately(_entityGroundNormal.x, -1) && Approximately(_entityGroundNormal.y, 0))
			{
				_rightObject = Physics2D.Raycast(new Vector2(entity.transform.position.x - _rayCastOffset.y, entity.transform.position.y + _rayCastOffset.x), Vector2.up, _objectCheckDistance, layerMask);
				_leftObject = Physics2D.Raycast(new Vector2(entity.transform.position.x - _rayCastOffset.y, entity.transform.position.y - _rayCastOffset.x), Vector2.down, _objectCheckDistance, layerMask);

				Debug.DrawRay(new Vector2(entity.transform.position.x - _rayCastOffset.y, entity.transform.position.y + _rayCastOffset.x), Vector2.up, Color.yellow);
				Debug.DrawRay(new Vector2(entity.transform.position.x - _rayCastOffset.y, entity.transform.position.y - _rayCastOffset.x), Vector2.down, Color.yellow);
			}

			Collider2D collider = null;
			
			if (_rightObject.collider != null)
			{
				collider = _rightObject.collider;
			}
			else if (_leftObject.collider != null)
			{
				collider = _leftObject.collider;
			}

			if (collider != null)
			{
				if (collider.gameObject.layer == LayerMask.NameToLayer("Static Obstacle") && alreadyCheckedGameObjectIDs.Count > 1)
				{
					return true;
				}
				else if (collider.gameObject.layer == LayerMask.NameToLayer("Creature") || collider.gameObject.layer == LayerMask.NameToLayer("Creature Obstacle"))
				{
					BaseEntity colliderEntity = collider.GetComponent<BaseEntity>();

					if (colliderEntity != null && !alreadyCheckedGameObjectIDs.Contains(colliderEntity.GetInstanceID()))
					{
						alreadyCheckedGameObjectIDs.Add(entity.GetInstanceID());

						recursiveCollision = ShouldTurnAroundFromCollision(colliderEntity, _layerMaskCreature, alreadyCheckedGameObjectIDs);

						if (!recursiveCollision)
						{
							recursiveCollision = ShouldTurnAroundFromCollision(colliderEntity, _layerMaskCreatureObstacle, alreadyCheckedGameObjectIDs);
						}

						if (!recursiveCollision)
						{
							recursiveCollision = ShouldTurnAroundFromCollision(colliderEntity, _layerMaskStaticObstacle, alreadyCheckedGameObjectIDs);
						}
					}
				}
			}
		}

		return recursiveCollision;
	}
}
