﻿public class DamageEntityOnTouchStrategy : Strategy
{
    private readonly BaseEntity _entity;
    private readonly int _damageToDeal;

    public DamageEntityOnTouchStrategy(BaseEntity entity, int damageToDeal)
    {
        _entity = entity;
        _damageToDeal = damageToDeal;
    }

    public override void Update() { }

    protected override void OnStart()
    {
        _entity.EntityEntered += OnEntityEntered;
        _entity.EntityExited += OnEntityExited;
    }

    protected override void OnStop()
    {
        _entity.EntityEntered -= OnEntityEntered;
        _entity.EntityExited -= OnEntityExited;
    }

    private void OnEntityEntered(BaseEntity entity)
    {
        if (entity.Velocity.y >= 0)
        {
            entity.TakeDamage?.Invoke(_damageToDeal, _entity);
        }
    }

    private void OnEntityExited(BaseEntity entity)
    {

    }
}