﻿using UnityEngine;

public class EntityAIWalkStrategy : Strategy
{
    private readonly CreatureEntity _entity;
    private readonly bool _startRight;

    public EntityAIWalkStrategy(CreatureEntity entity, bool startRight)
    {
        _entity = entity;
        _startRight = startRight;
    }

    protected override void OnStart()
    {
        _entity.SetFacingDirection(_startRight ? Vector2.right : Vector2.left, true);
    }

    protected override void OnStop()
    {
        _entity.CharacterController.ResetVelocity();
    }

    public override void Update()
    {
        if (_entity.ControlledBy == null)
        {
            _entity.Brain.Right = _entity.FacingDirection.x > 0;
            _entity.Brain.Left = _entity.FacingDirection.x < 0;
        }
        else
        {
            _entity.Brain.Right = false;
            _entity.Brain.Left = false;
        }
    }
}