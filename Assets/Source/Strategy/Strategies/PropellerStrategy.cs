﻿using Lightbug.Kinematic2D.Core;
using Lightbug.Kinematic2D.Implementation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropellerStrategy : Strategy
{
	private readonly BaseEntity _entity;

	private CharacterMotor _characterMotor;
	private CharacterController2D _characterController;

	private float verticalVelocity = 0f;

	public PropellerStrategy(BaseEntity entity)
	{
		_entity = entity;
	}

	protected override void OnStart()
	{
	}

	protected override void OnStop()
	{
	}

	public override void Update()
	{
		if (_characterMotor != null && verticalVelocity != 0f)
		{
			_characterMotor.ForceNotGroundedState();
			_characterController.MovementController.SetState(MovementState.Normal);
			_characterMotor.SetVelocityY(verticalVelocity);
		}
	}

	private void StartFlying()
	{
		verticalVelocity = 2f;
	}

	private void StopFlying()
	{
		verticalVelocity = 0f;
	}

	public override void OwnerEquipped(BaseEntity entity)
	{
		Activate();

		StopFlying();

		_characterMotor = entity.GetComponent<CharacterMotor>();
		_characterController = entity.GetComponent<CharacterController2D>();
	}

	public override void OwnerUnequipped(BaseEntity entity)
	{
		StopFlying();

		_characterMotor = _entity.GetComponent<CharacterMotor>();
		_characterController = _entity.GetComponent<CharacterController2D>();

		Deactivate();
	}

	public override void ActivateAbility(BaseEntity entity)
	{
		StartFlying();
	}

	public override void DeactivateAbility(BaseEntity entity)
	{
		StopFlying();
	}
}
