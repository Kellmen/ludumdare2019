﻿using UnityEngine;

public class ShootProjectileAtEntityStrategy : Strategy
{
    private readonly BaseEntity _entity;
    private readonly BaseEntity _target;
    private readonly GameObject _projectilePrefab;
    private readonly float _cooldown;

	bool fireAutomatically = true;

    private float TimeOfLastAction = 0f;

    public ShootProjectileAtEntityStrategy(BaseEntity entity, BaseEntity target, float cooldown, GameObject projectilePrefab)
    {
        _entity = entity;
        _target = target;
        _cooldown = cooldown;
        _projectilePrefab = projectilePrefab;

		for (int i = 0; i < 4; i++)
		{
			GameObject goProjectile = GameObject.Instantiate(_projectilePrefab);

			BaseGame.Instance.ObjectPool.AddObjectToPool(goProjectile.GetComponent<PoolableObject>());
		}
	}

    protected override void OnStart()
    {
        TimeOfLastAction = Time.time;
	}

    protected override void OnStop()
    {
    }

    public override void Update()
	{
		if (fireAutomatically)
		{
			if (_target.GetPosition().x < _entity.gameObject.transform.position.x && _entity.FacingDirection.x > 0f)
			{
				_entity.SetFacingDirection(Vector2.left, false);
			}
			else if (_target.GetPosition().x > _entity.gameObject.transform.position.x && _entity.FacingDirection.x < 0f)
			{
				_entity.SetFacingDirection(Vector2.right, false);
			}

			if (Time.time > TimeOfLastAction + _cooldown)
			{
				ShootProjectile(_entity);
			}
		}
    }

    public void ShootProjectile(BaseEntity entity, float horizontalOffsetMultiplier = 1f)
	{
		PoolableObject projectilePoolableObject = BaseGame.Instance.ObjectPool.GetObjectFromPool(PooledObjectIdentifiers.BaseProjectile);//, entity.transform);
		IProjectile projectile = projectilePoolableObject.GetComponent<IProjectile>();

        Vector2 spawnOffset = new Vector2(entity.FacingDirection.x * 0.26f * horizontalOffsetMultiplier, 0.4f);
        Vector2 spawnPosition = (Vector2)entity.gameObject.transform.position + spawnOffset;
        Vector2 initialVelocity = entity.FacingDirection * projectile.GetSpeed();

        projectile.Fire(spawnPosition, initialVelocity);

        TimeOfLastAction = Time.time;
	}

	public override void OwnerEquipped(BaseEntity entity)
	{
		TimeOfLastAction = 0f;
		fireAutomatically = false;
	}

	public override void OwnerUnequipped(BaseEntity entity)
	{
		TimeOfLastAction = Time.time;
		fireAutomatically = true;
	}

	public override void ActivateAbility(BaseEntity entity)
	{
		if (Time.time > TimeOfLastAction + _cooldown)
		{
			ShootProjectile(entity, 1.25f);
		}
	}
}