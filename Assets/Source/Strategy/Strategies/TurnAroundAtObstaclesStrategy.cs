﻿using UnityEngine;

public class TurnAroundAtObstaclesStrategy : Strategy
{
    private readonly BaseEntity _entity;
    private readonly Vector2 _rayCastOffset;

    private RaycastHit2D _rightWall;
    private RaycastHit2D _leftWall;
    private RaycastHit2D _rightLedge;
    private RaycastHit2D _leftLedge;

    private Vector3 _entityGroundNormal;

    private LayerMask _layerMask = LayerMask.GetMask("Static Obstacle");
    float _wallCheckDistance = 0.08f;
    float _ledgeCheckDistance = 0.5f;
    float _directionSwitchCooldown = 0.25f;
    float _directionSwitchWaitTime = 0f;
    float _tolerance = 0.001f;

    public TurnAroundAtObstaclesStrategy(CreatureEntity entity, Vector2 rayCastOffset)
    {
        _entity = entity;
        _rayCastOffset = rayCastOffset;
    }

    protected override void OnStart() { }

    protected override void OnStop() { }

    private bool Approximately(float floatOne, float floatTwo)
    {
        return Mathf.Abs(floatOne - floatTwo) <= _tolerance;
    }

    public override void Update()
    {
        if (_entity.ControlledBy == null && _entity.CharacterController.IsGrounded && Time.time > _directionSwitchWaitTime)
        {
            _entityGroundNormal = _entity.CharacterController.GroundNormal;

			// up
			if (Approximately(_entityGroundNormal.x, 0) && Approximately(_entityGroundNormal.y, 1))
            {
                _rightWall = Physics2D.Raycast(new Vector2(_entity.transform.position.x + _rayCastOffset.x, _entity.transform.position.y + _rayCastOffset.y), Vector2.right, _wallCheckDistance, _layerMask);
                _leftWall = Physics2D.Raycast(new Vector2(_entity.transform.position.x - _rayCastOffset.x, _entity.transform.position.y + _rayCastOffset.y), Vector2.left, _wallCheckDistance, _layerMask);
                _rightLedge = Physics2D.Raycast(new Vector2(_entity.transform.position.x + _rayCastOffset.x, _entity.transform.position.y), Vector2.down, _ledgeCheckDistance);
                _leftLedge = Physics2D.Raycast(new Vector2(_entity.transform.position.x - _rayCastOffset.x, _entity.transform.position.y), Vector2.down, _ledgeCheckDistance);

                Debug.DrawRay(new Vector2(_entity.transform.position.x + _rayCastOffset.x, _entity.transform.position.y + _rayCastOffset.y), Vector2.right, Color.red);
                Debug.DrawRay(new Vector2(_entity.transform.position.x - _rayCastOffset.x, _entity.transform.position.y + _rayCastOffset.y), Vector2.left, Color.red);
                Debug.DrawRay(new Vector2(_entity.transform.position.x + _rayCastOffset.x, _entity.transform.position.y), Vector2.down, Color.red);
                Debug.DrawRay(new Vector2(_entity.transform.position.x - _rayCastOffset.x, _entity.transform.position.y), Vector2.down, Color.red);
            }
            // down
            if (Approximately(_entityGroundNormal.x, 0) && Approximately(_entityGroundNormal.y, -1))
            {
                _rightWall = Physics2D.Raycast(new Vector2(_entity.transform.position.x - _rayCastOffset.x, _entity.transform.position.y - _rayCastOffset.y), Vector2.left, _wallCheckDistance, _layerMask);
                _leftWall = Physics2D.Raycast(new Vector2(_entity.transform.position.x + _rayCastOffset.x, _entity.transform.position.y - _rayCastOffset.y), Vector2.right, _wallCheckDistance, _layerMask);
                _rightLedge = Physics2D.Raycast(new Vector2(_entity.transform.position.x - _rayCastOffset.x, _entity.transform.position.y), Vector2.up, _ledgeCheckDistance);
                _leftLedge = Physics2D.Raycast(new Vector2(_entity.transform.position.x + _rayCastOffset.x, _entity.transform.position.y), Vector2.up, _ledgeCheckDistance);

                Debug.DrawRay(new Vector2(_entity.transform.position.x - _rayCastOffset.x, _entity.transform.position.y - _rayCastOffset.y), Vector2.left, Color.blue);
                Debug.DrawRay(new Vector2(_entity.transform.position.x + _rayCastOffset.x, _entity.transform.position.y - _rayCastOffset.y), Vector2.right, Color.blue);
                Debug.DrawRay(new Vector2(_entity.transform.position.x - _rayCastOffset.x, _entity.transform.position.y), Vector2.up, Color.blue);
                Debug.DrawRay(new Vector2(_entity.transform.position.x + _rayCastOffset.x, _entity.transform.position.y), Vector2.up, Color.blue);
			}
            // right
            if (Approximately(_entityGroundNormal.x, 1) && Approximately(_entityGroundNormal.y, 0))
            {
                _rightWall = Physics2D.Raycast(new Vector2(_entity.transform.position.x + _rayCastOffset.y, _entity.transform.position.y - _rayCastOffset.x), Vector2.down, _wallCheckDistance, _layerMask);
                _leftWall = Physics2D.Raycast(new Vector2(_entity.transform.position.x + _rayCastOffset.y, _entity.transform.position.y + _rayCastOffset.x), Vector2.up, _wallCheckDistance, _layerMask);
                _rightLedge = Physics2D.Raycast(new Vector2(_entity.transform.position.x, _entity.transform.position.y - _rayCastOffset.x), Vector2.left, _ledgeCheckDistance);
                _leftLedge = Physics2D.Raycast(new Vector2(_entity.transform.position.x, _entity.transform.position.y + _rayCastOffset.x), Vector2.left, _ledgeCheckDistance);

                Debug.DrawRay(new Vector2(_entity.transform.position.x + _rayCastOffset.y, _entity.transform.position.y - _rayCastOffset.x), Vector2.down, Color.green);
                Debug.DrawRay(new Vector2(_entity.transform.position.x + _rayCastOffset.y, _entity.transform.position.y + _rayCastOffset.x), Vector2.up, Color.green);
                Debug.DrawRay(new Vector2(_entity.transform.position.x, _entity.transform.position.y - _rayCastOffset.x), Vector2.left, Color.green);
                Debug.DrawRay(new Vector2(_entity.transform.position.x, _entity.transform.position.y + _rayCastOffset.x), Vector2.left, Color.green);
			}
            // left
            if (Approximately(_entityGroundNormal.x, -1) && Approximately(_entityGroundNormal.y, 0))
            {
                _rightWall = Physics2D.Raycast(new Vector2(_entity.transform.position.x - _rayCastOffset.y, _entity.transform.position.y + _rayCastOffset.x), Vector2.up, _wallCheckDistance, _layerMask);
                _leftWall = Physics2D.Raycast(new Vector2(_entity.transform.position.x - _rayCastOffset.y, _entity.transform.position.y - _rayCastOffset.x), Vector2.down, _wallCheckDistance, _layerMask);
                _rightLedge = Physics2D.Raycast(new Vector2(_entity.transform.position.x, _entity.transform.position.y + _rayCastOffset.x), Vector2.right, _ledgeCheckDistance);
                _leftLedge = Physics2D.Raycast(new Vector2(_entity.transform.position.x, _entity.transform.position.y - _rayCastOffset.x), Vector2.right, _ledgeCheckDistance);

                Debug.DrawRay(new Vector2(_entity.transform.position.x - _rayCastOffset.y, _entity.transform.position.y + _rayCastOffset.x), Vector2.up, Color.yellow);
                Debug.DrawRay(new Vector2(_entity.transform.position.x - _rayCastOffset.y, _entity.transform.position.y - _rayCastOffset.x), Vector2.down, Color.yellow);
                Debug.DrawRay(new Vector2(_entity.transform.position.x, _entity.transform.position.y + _rayCastOffset.x), Vector2.right, Color.yellow);
                Debug.DrawRay(new Vector2(_entity.transform.position.x, _entity.transform.position.y - _rayCastOffset.x), Vector2.right, Color.yellow);
			}

            //Check for walls
            if (_rightWall.collider != null)
            {
                _entity.SetFacingDirection(Vector2.left, false);
                _directionSwitchWaitTime = Time.time + _directionSwitchCooldown;
            }

            if (_leftWall.collider != null)
            {
                _entity.SetFacingDirection(Vector2.right, false);
                _directionSwitchWaitTime = Time.time + _directionSwitchCooldown;
            }

            // Check for ledges
            if (_rightLedge.collider == null)
            {
                _entity.SetFacingDirection(Vector2.left, false);
                _directionSwitchWaitTime = Time.time + _directionSwitchCooldown;
            }

            if (_leftLedge.collider == null)
            {
                _entity.SetFacingDirection(Vector2.right, false);
                _directionSwitchWaitTime = Time.time + _directionSwitchCooldown;
            }
        }
    }
}