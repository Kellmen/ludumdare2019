﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseEquippedEntityStrategy : Strategy
{
	private readonly BaseEntity _entity;
	private readonly InControlInputController _input;
	private readonly Vector2 _originOffset;
	private readonly Transform _holdAnchor;
	private readonly Transform _entityTransform;

	private Strategy _equippedStrategy;

	public UseEquippedEntityStrategy(BaseEntity entity, InControlInputController input, Vector2 originOffset, Transform holdAnchor)
	{
		_entity = entity;
		_input = input;
		_originOffset = originOffset;
		_holdAnchor = holdAnchor;

		_entityTransform = _entity.gameObject.transform;
	}

	protected override void OnStart()
	{
	}

	protected override void OnStop()
	{
	}

	public override void Update()
	{
		if (_equippedStrategy != null)
		{
			if (_input.CharacterAction.Ability1WasPressed)
			{
				_equippedStrategy.ActivateAbility(_entity);
			}
			else if (_input.CharacterAction.Ability1WasReleased)
			{
				_equippedStrategy.DeactivateAbility(_entity);
			}
		}
	}

	//public BaseEntity GetEquippedEntity()
	//{
	//	return _equippedEntity;
	//}
	//
	//public void SetEquippedEntity(BaseEntity equippedEntity)
	//{
	//	_equippedStrategy = equippedEntity;
	//}

	public Strategy GetEquippedStrategy()
	{
		return _equippedStrategy;
	}

	public void SetEquippedStrategy(Strategy equippedStrategy)
	{
		_equippedStrategy = equippedStrategy;
	}
}
