﻿using UnityEngine;

public abstract class Strategy
{
    public bool Active { get; private set; }

    public void Activate()
    {
        if (!Active)
        {
            //Debug.Log($"Activating Strategy: {GetType().Name}");
            Active = true;
            OnStart();
        }
    }
    public void Deactivate()
    {
        //Debug.Log($"Deactivating Strategy: {GetType().Name}");
        if (Active)
        {
            Active = false;
            OnStop();
        }
    }

    public abstract void Update();

    protected abstract void OnStart();
    protected abstract void OnStop();

	public virtual void OwnerEquipped(BaseEntity entity) { }
	public virtual void OwnerUnequipped(BaseEntity entity) { }

	public virtual void ActivateAbility(BaseEntity entity) { }
	public virtual void DeactivateAbility(BaseEntity entity) { }
}