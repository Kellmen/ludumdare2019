﻿using System.Collections.Generic;

public class StrategyManager
{
    private List<Strategy> _strategies = new List<Strategy>();

    public void Add(Strategy strategy)
    {
        _strategies.Add(strategy);
        strategy.Activate();
    }

    public void Remove(Strategy strategy)
    {
        strategy.Deactivate();
        _strategies.Remove(strategy);
    }

    public void Update()
    {
        foreach (var strategy in _strategies)
        {
            if (strategy.Active) strategy.Update();
        }
    }
}