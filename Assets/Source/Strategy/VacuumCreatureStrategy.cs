﻿using UnityEngine;
using System.Collections.Generic;
using Lightbug.Kinematic2D.Implementation;
using System.Collections;

public class VacuumCreatureStrategy : Strategy
{
    private readonly BaseEntity _entity;
    private readonly InControlInputController _input;
    private readonly Vector2 _originOffset;
    private readonly float _coneDistance;
    private readonly float _coneSize;
    private readonly Transform _entityTransform;
    private readonly Transform _holdAnchor;
    private readonly float _raySpacing = 0.125f;

    private List<RaycastHit2D> _raycasts = new List<RaycastHit2D>();

    private Vector2 _entityDirectionOnVacuumStart;
    private CreatureEntity _vacuumTarget = null;
    private float _vacuumSpeed = 5.0f;

    private bool _buttonReleased;
    private bool _holdingObject;
    private bool _drawGizmos;
    private bool _dropKeyReleased = true;

    private IEnumerator _shootTarget;

    public VacuumCreatureStrategy(BaseEntity entity, InControlInputController input, Vector2 originOffset, Transform holdAnchor, float coneDistance, float coneSize)
    {
        _entity = entity;
        _input = input;
        _originOffset = originOffset;
        _holdAnchor = holdAnchor;
        _coneDistance = coneDistance;
        _coneSize = coneSize;

        _entityTransform = _entity.gameObject.transform;
    }

    protected override void OnStart()
    {
        _entity.DrawGizmos += HandleDrawGizmos;
    }

    protected override void OnStop()
    {
        _entity.DrawGizmos -= HandleDrawGizmos;

        _shootTarget = null;
    }

    public override void Update()
	{
		if (_shootTarget != null && !_shootTarget.MoveNext())
		{
			_shootTarget = null;
		}

		if (_input.CharacterAction.vacuumIsPressed && !_holdingObject && _dropKeyReleased)
		{
			_drawGizmos = true;

			if (_vacuumTarget != null)
			{
				if (_entity.FacingDirection == _entityDirectionOnVacuumStart)
				{
					float interpolation = _vacuumSpeed * Time.deltaTime;
					Vector3 moveTo = _holdAnchor.position - _vacuumTarget.CharacterController.Position;

					_vacuumTarget.VerticalControl.isEnabled = false;
					_vacuumTarget.HorizontalControl.isEnabled = false;
					_vacuumTarget.CharacterController.AlignCharacterTowardsUp();

					_vacuumTarget.CharacterController.verticalAlignmentSettings.worldVerticalDirection = new Vector3(0, 1, 0);
					if (_vacuumTarget.CornerAlign != null) _vacuumTarget.CornerAlign.isEnabled = false;
					if (_vacuumTarget.WallAlign != null) _vacuumTarget.WallAlign.isEnabled = false;
					_vacuumTarget.SetControlledBy(_entity);

					_vacuumTarget.CharacterController.Move(moveTo * interpolation, false, true);

					if ((_holdAnchor.position - _vacuumTarget.CharacterController.Position).sqrMagnitude < 0.5f)
					{
						if (_vacuumTarget.EquippedActionStrategy != null)
						{
							((PlayerEntity)_entity).SetEquippedEntityStrategy(_vacuumTarget.EquippedActionStrategy);
							_vacuumTarget.EquippedActionStrategy.OwnerEquipped(_entity);
						}

						_vacuumTarget.CharacterController.CharacterBody.transform.position = _holdAnchor.transform.position;
						_vacuumTarget.CharacterController.CharacterBody.transform.SetParent(_holdAnchor.transform);
						_vacuumTarget.CharacterGraphics.gameObject.SetActive(false);
						_vacuumTarget.gameObject.SetActive(false);

						_holdingObject = true;
					}
				}
				else
				{
					_drawGizmos = false;
					_dropKeyReleased = false;
					DropObject();
				}
			}
			else
			{
				// Is using this okay???
				Physics2D.queriesStartInColliders = false;

				_raycasts.Clear();
				_raycasts.Add(Physics2D.Raycast((Vector2)_entityTransform.position + _originOffset, _entity.FacingDirection, _coneDistance));

				float size = _raySpacing;
				while (size < _coneSize)
				{
					_raycasts.Add(Physics2D.Raycast((Vector2)_entityTransform.position + _originOffset, new Vector2(_entity.FacingDirection.x, size), _coneDistance));
					_raycasts.Add(Physics2D.Raycast((Vector2)_entityTransform.position + _originOffset, new Vector2(_entity.FacingDirection.x, -size), _coneDistance));

					size += _raySpacing;
				}

				float nearestDistance = float.MaxValue;
				foreach (RaycastHit2D hit in _raycasts)
				{
					if (hit.collider != null)
					{
						float hitDistance = (_entityTransform.position - hit.collider.transform.position).sqrMagnitude;

						if (hitDistance < nearestDistance)
						{
							CreatureEntity baseEntity = hit.collider.gameObject.GetComponent<CreatureEntity>();

							if (baseEntity != null)
							{
								_entityDirectionOnVacuumStart = _entity.FacingDirection;
								_vacuumTarget = baseEntity;
								nearestDistance = hitDistance;
							}
						}
					}
				}

				Physics2D.queriesStartInColliders = true;
			}
		}
		else if (_input.CharacterAction.vacuumWasReleased)
		{
			_drawGizmos = false;
			_dropKeyReleased = true;

			if (!_holdingObject)
			{
				DropObject();
			}
		}
		else if (_input.CharacterAction.vacuumWasPressed)
		{
			_drawGizmos = false;

			if (_holdingObject && _shootTarget == null)
			{
				ShootObject(new Vector2(0, 0));
				_dropKeyReleased = false;
			}
		}
		else if (_input.CharacterAction.shootWasPressed)
		{
			if (_holdingObject && _shootTarget == null)
			{
				ShootObject(new Vector2(10, 3));
			}
		}
    }

    private void DropObject()
    {
        if (_vacuumTarget != null)
        {
            ReEnableVacuumTarget();
            _vacuumTarget.CharacterController.ResetVelocity();
            if (_vacuumTarget.CornerAlign != null) _vacuumTarget.CornerAlign.isEnabled = true;
            if (_vacuumTarget.WallAlign != null) _vacuumTarget.WallAlign.isEnabled = true;
            _vacuumTarget.HorizontalControl.isEnabled = true;
            _vacuumTarget.SetControlledBy(null);

            _vacuumTarget = null;
        }

        _holdingObject = false;
        _drawGizmos = false;
    }

    private void ShootObject(Vector2 shootVector)
    {
        ReEnableVacuumTarget();
        _drawGizmos = false;
        _shootTarget = DoShootTarget(shootVector);
    }

    private void ReEnableVacuumTarget()
    {
        _vacuumTarget.VerticalControl.isEnabled = true;
        _vacuumTarget.CharacterGraphics.gameObject.SetActive(true);
        _vacuumTarget.CharacterController.CharacterBody.transform.SetParent(null);
        _vacuumTarget.gameObject.SetActive(true);

		if (_vacuumTarget.EquippedActionStrategy != null)
		{
			((PlayerEntity)_entity).SetEquippedEntityStrategy(null);
			_vacuumTarget.EquippedActionStrategy.OwnerUnequipped(_entity);
		}
	}

    private IEnumerator DoShootTarget(Vector2 shootVector)
    {
        _vacuumTarget.CharacterController.ResetVelocity();
        _vacuumTarget.CharacterController.AlignCharacterTowardsUp();

        _vacuumTarget.CharacterController.OnLeftCollision += HandleLeftCollision;
        _vacuumTarget.CharacterController.OnRightCollision += HandleRightCollision;
        _vacuumTarget.CharacterController.OnGroundCollision += HandleGroundCollision;

        _vacuumTarget.SetFacingDirection(_entity.FacingDirection, true);
        _vacuumTarget.CharacterController.AddVelocityY(shootVector.y);
        _vacuumTarget.CharacterController.AddVelocityX(_entity.FacingDirection.x * shootVector.x);

        float newVel = _entity.FacingDirection.x * -0.1f;
        while (Mathf.Abs(_vacuumTarget.CharacterController.Velocity.x) > Mathf.Abs(newVel))
        {
            _vacuumTarget.CharacterController.AddVelocityX(newVel);

            yield return null;
        }

        _vacuumTarget.CharacterController.OnLeftCollision -= HandleLeftCollision;
        _vacuumTarget.CharacterController.OnRightCollision -= HandleRightCollision;
        _vacuumTarget.CharacterController.OnGroundCollision -= HandleGroundCollision;

        _vacuumTarget.CharacterController.ResetVelocity();
        if (_vacuumTarget.CornerAlign != null) _vacuumTarget.CornerAlign.isEnabled = true;
        if (_vacuumTarget.WallAlign != null) _vacuumTarget.WallAlign.isEnabled = true;
        _vacuumTarget.HorizontalControl.isEnabled = true;
        _vacuumTarget.SetControlledBy(null);

        _vacuumTarget = null;
        _holdingObject = false;

        yield return null;
    }

    private void HandleGroundCollision()
    {
        if (_vacuumTarget != null)
        {
            _vacuumTarget.CharacterController.OnLeftCollision -= HandleLeftCollision;
            _vacuumTarget.CharacterController.OnRightCollision -= HandleRightCollision;
            _vacuumTarget.CharacterController.OnGroundCollision -= HandleGroundCollision;
        }
    }

    private void HandleRightCollision()
    {
        HandleShootCollision(new Vector3(-1, -1, 0));
    }

    private void HandleLeftCollision()
    {
        HandleShootCollision(new Vector3(1, -1, 0));
    }

    private void HandleShootCollision(Vector3 alignment)
    {
        if (_vacuumTarget == null)
            return;

        _vacuumTarget.CharacterController.OnLeftCollision -= HandleLeftCollision;
        _vacuumTarget.CharacterController.OnRightCollision -= HandleRightCollision;
        _vacuumTarget.CharacterController.OnGroundCollision -= HandleGroundCollision;

        if (_vacuumTarget.SnapToWalls)
        {
            _vacuumTarget.CharacterController.verticalAlignmentSettings.worldVerticalDirection = alignment;
        }

        _vacuumTarget.CharacterController.ResetVelocity();
        if (_vacuumTarget.CornerAlign != null) _vacuumTarget.CornerAlign.isEnabled = true;
        if (_vacuumTarget.WallAlign != null) _vacuumTarget.WallAlign.isEnabled = true;
        _vacuumTarget.HorizontalControl.isEnabled = true;
        _vacuumTarget.SetControlledBy(null);

        _vacuumTarget = null;
        _holdingObject = false;

        _shootTarget = null;
    }

    void HandleDrawGizmos()
    {
        if (!_drawGizmos) return;

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(_entityTransform.position + (Vector3)_originOffset, _entityTransform.position + (new Vector3(_entity.FacingDirection.x * _coneDistance, 0, 0)));

        float size = _raySpacing;
        while (size < _coneSize)
        {
            Gizmos.DrawLine(_entityTransform.position + (Vector3)_originOffset, _entityTransform.position + (new Vector3(_entity.FacingDirection.x * _coneDistance, size, 0)));
            Gizmos.DrawLine(_entityTransform.position + (Vector3)_originOffset, _entityTransform.position + (new Vector3(_entity.FacingDirection.x * _coneDistance, -size, 0)));

            size += _raySpacing;
        }
    }

	public CreatureEntity GetVacuumEntity()
	{
		return _vacuumTarget;
	}
}