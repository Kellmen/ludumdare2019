﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuButton : MonoBehaviour
{
    [SerializeField]
    private Button _button;

    public void HandleButtonClicked()
    {
        _button.interactable = false;
        BaseGame.Instance.SwitchState(new MainMenuGameState());
    }
}