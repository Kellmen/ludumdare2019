﻿using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    [SerializeField]
    private Transform _cameraTransform;

    [SerializeField]
    private float _parallaxSpeed;

    [SerializeField]
    private SpriteRenderer _referenceSprite;

    private Transform[] _layers;

    private float _viewZone = 10;
    private float _lastCameraX;
    private float _backgroundSize;

    private int _leftIndex;
    private int _rightIndex;

    private void Start()
    {
        _lastCameraX = _cameraTransform.position.x;

        _layers = new Transform[transform.childCount];

        for (int i = 0; i < transform.childCount; ++i)
        {
            _layers[i] = transform.GetChild(i);
        }

        _backgroundSize = _referenceSprite.bounds.size.x;

        _leftIndex = 0;
        _rightIndex = _layers.Length - 1;
    }

    private void LateUpdate()
    {
        float deltaX = _cameraTransform.position.x - _lastCameraX;
        transform.position += Vector3.right * (deltaX * _parallaxSpeed);

        _lastCameraX = _cameraTransform.position.x;

        if (_cameraTransform.position.x < (_layers[_leftIndex].transform.position.x + _viewZone))
        {
            ScrollLeft();
        }

        if (_cameraTransform.position.x > (_layers[_rightIndex].transform.position.x - _viewZone))
        {
            ScrollRight();
        }
    }

    private void ScrollLeft()
    {
        _layers[_rightIndex].position = new Vector3(_layers[_leftIndex].position.x - _backgroundSize, _layers[_leftIndex].position.y, _layers[_leftIndex].position.z);
        _leftIndex = _rightIndex;
        _rightIndex--;

        if (_rightIndex < 0)
        {
            _rightIndex = _layers.Length - 1;
        }
    }

    private void ScrollRight()
    {
        _layers[_leftIndex].position = new Vector3(_layers[_rightIndex].position.x + _backgroundSize, _layers[_rightIndex].position.y, _layers[_rightIndex].position.z);
        _rightIndex = _leftIndex;
        _leftIndex++;

        if (_leftIndex >= _layers.Length)
        {
            _leftIndex = 0;
        }
    }
}