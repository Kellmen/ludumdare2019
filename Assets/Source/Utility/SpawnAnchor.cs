﻿using UnityEngine;

public class SpawnAnchor : MonoBehaviour
{
    private Transform _target;
    private Vector3 _startPosition;

    private float _timeToWaitToFollow = 0.1f;
    private float _waitTimeStart;

    private void Awake()
    {
        _startPosition = this.transform.position;
    }

    private void Update()
    {
        if (_target != null && Time.time > _waitTimeStart)
        {
            transform.position = Vector3.Lerp(transform.position,_target.position, 0.01f);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, _startPosition, 0.01f);
        }
    }

    public void SetTarget(Transform target)
    {
        _waitTimeStart = Time.time + _timeToWaitToFollow;
        _target = target;
    }
}